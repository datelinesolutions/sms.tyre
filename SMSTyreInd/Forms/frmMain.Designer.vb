﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.Ribbon1 = New System.Windows.Forms.Ribbon()
        Me.RibbonTab1 = New System.Windows.Forms.RibbonTab()
        Me.RibbonPanel1 = New System.Windows.Forms.RibbonPanel()
        Me.cmdNew = New System.Windows.Forms.RibbonButton()
        Me.cmdEdit = New System.Windows.Forms.RibbonButton()
        Me.RibbonButton5 = New System.Windows.Forms.RibbonButton()
        Me.cmdDelete = New System.Windows.Forms.RibbonButton()
        Me.RPAction = New System.Windows.Forms.RibbonPanel()
        Me.cmdUndo = New System.Windows.Forms.RibbonButton()
        Me.cmdSave = New System.Windows.Forms.RibbonButton()
        Me.RibbonPanel3 = New System.Windows.Forms.RibbonPanel()
        Me.cmdFirst = New System.Windows.Forms.RibbonButton()
        Me.cmdPrevious = New System.Windows.Forms.RibbonButton()
        Me.cmdNext = New System.Windows.Forms.RibbonButton()
        Me.cmdLast = New System.Windows.Forms.RibbonButton()
        Me.RibbonPanel2 = New System.Windows.Forms.RibbonPanel()
        Me.cmdFind = New System.Windows.Forms.RibbonButton()
        Me.cmdPrint = New System.Windows.Forms.RibbonButton()
        Me.RPExit = New System.Windows.Forms.RibbonPanel()
        Me.cmdExitForm = New System.Windows.Forms.RibbonButton()
        Me.mnuMain = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChangePasswordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BrandToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TyreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PatternToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.VendorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.BanksToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WarehouseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TransactionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PurcahseOrderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalesInvoiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.PaymentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.POReceivedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'Ribbon1
        '
        Me.Ribbon1.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Ribbon1.Location = New System.Drawing.Point(0, 24)
        Me.Ribbon1.Name = "Ribbon1"
        '
        '
        '
        Me.Ribbon1.OrbDropDown.BorderRoundness = 8
        Me.Ribbon1.OrbDropDown.Location = New System.Drawing.Point(0, 0)
        Me.Ribbon1.OrbDropDown.Name = ""
        Me.Ribbon1.OrbDropDown.RecentItemsCaption = Nothing
        Me.Ribbon1.OrbDropDown.Size = New System.Drawing.Size(527, 72)
        Me.Ribbon1.OrbDropDown.TabIndex = 0
        Me.Ribbon1.OrbImage = Nothing
        Me.Ribbon1.OrbText = Nothing
        '
        '
        '
        Me.Ribbon1.QuickAcessToolbar.AltKey = Nothing
        Me.Ribbon1.QuickAcessToolbar.CheckedGroup = Nothing
        Me.Ribbon1.QuickAcessToolbar.Image = Nothing
        Me.Ribbon1.QuickAcessToolbar.Tag = Nothing
        Me.Ribbon1.QuickAcessToolbar.Text = Nothing
        Me.Ribbon1.QuickAcessToolbar.ToolTip = Nothing
        Me.Ribbon1.QuickAcessToolbar.ToolTipTitle = Nothing
        Me.Ribbon1.QuickAcessToolbar.Value = Nothing
        Me.Ribbon1.Size = New System.Drawing.Size(1020, 142)
        Me.Ribbon1.TabIndex = 1
        Me.Ribbon1.Tabs.Add(Me.RibbonTab1)
        Me.Ribbon1.TabsMargin = New System.Windows.Forms.Padding(12, 26, 20, 0)
        Me.Ribbon1.TabSpacing = 6
        Me.Ribbon1.Text = "Ribbon1"
        '
        'RibbonTab1
        '
        Me.RibbonTab1.Panels.Add(Me.RibbonPanel1)
        Me.RibbonTab1.Panels.Add(Me.RPAction)
        Me.RibbonTab1.Panels.Add(Me.RibbonPanel3)
        Me.RibbonTab1.Panels.Add(Me.RibbonPanel2)
        Me.RibbonTab1.Panels.Add(Me.RPExit)
        Me.RibbonTab1.Tag = Nothing
        Me.RibbonTab1.Text = "Form Actions"
        Me.RibbonTab1.ToolTip = Nothing
        Me.RibbonTab1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.None
        Me.RibbonTab1.ToolTipImage = Nothing
        Me.RibbonTab1.ToolTipTitle = Nothing
        Me.RibbonTab1.Value = Nothing
        '
        'RibbonPanel1
        '
        Me.RibbonPanel1.ButtonMoreEnabled = False
        Me.RibbonPanel1.ButtonMoreVisible = False
        Me.RibbonPanel1.Items.Add(Me.cmdNew)
        Me.RibbonPanel1.Items.Add(Me.cmdEdit)
        Me.RibbonPanel1.Items.Add(Me.cmdDelete)
        Me.RibbonPanel1.Tag = Nothing
        Me.RibbonPanel1.Text = ""
        '
        'cmdNew
        '
        Me.cmdNew.AltKey = Nothing
        Me.cmdNew.CheckedGroup = Nothing
        Me.cmdNew.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down
        Me.cmdNew.DropDownArrowSize = New System.Drawing.Size(5, 3)
        Me.cmdNew.Enabled = False
        Me.cmdNew.Image = CType(resources.GetObject("cmdNew.Image"), System.Drawing.Image)
        Me.cmdNew.SmallImage = CType(resources.GetObject("cmdNew.SmallImage"), System.Drawing.Image)
        Me.cmdNew.Style = System.Windows.Forms.RibbonButtonStyle.Normal
        Me.cmdNew.Tag = Nothing
        Me.cmdNew.Text = "New"
        Me.cmdNew.ToolTip = Nothing
        Me.cmdNew.ToolTipTitle = "Insert New Record"
        Me.cmdNew.Value = Nothing
        '
        'cmdEdit
        '
        Me.cmdEdit.AltKey = Nothing
        Me.cmdEdit.CheckedGroup = Nothing
        Me.cmdEdit.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down
        Me.cmdEdit.DropDownArrowSize = New System.Drawing.Size(5, 3)
        Me.cmdEdit.DropDownItems.Add(Me.RibbonButton5)
        Me.cmdEdit.Enabled = False
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.SmallImage = CType(resources.GetObject("cmdEdit.SmallImage"), System.Drawing.Image)
        Me.cmdEdit.Style = System.Windows.Forms.RibbonButtonStyle.Normal
        Me.cmdEdit.Tag = Nothing
        Me.cmdEdit.Text = "Edit"
        Me.cmdEdit.ToolTip = Nothing
        Me.cmdEdit.ToolTipTitle = Nothing
        Me.cmdEdit.Value = Nothing
        '
        'RibbonButton5
        '
        Me.RibbonButton5.AltKey = Nothing
        Me.RibbonButton5.CheckedGroup = Nothing
        Me.RibbonButton5.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down
        Me.RibbonButton5.DropDownArrowSize = New System.Drawing.Size(5, 3)
        Me.RibbonButton5.Image = CType(resources.GetObject("RibbonButton5.Image"), System.Drawing.Image)
        Me.RibbonButton5.SmallImage = CType(resources.GetObject("RibbonButton5.SmallImage"), System.Drawing.Image)
        Me.RibbonButton5.Style = System.Windows.Forms.RibbonButtonStyle.Normal
        Me.RibbonButton5.Tag = Nothing
        Me.RibbonButton5.Text = "RibbonButton5"
        Me.RibbonButton5.ToolTip = Nothing
        Me.RibbonButton5.ToolTipTitle = Nothing
        Me.RibbonButton5.Value = Nothing
        '
        'cmdDelete
        '
        Me.cmdDelete.AltKey = Nothing
        Me.cmdDelete.CheckedGroup = Nothing
        Me.cmdDelete.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down
        Me.cmdDelete.DropDownArrowSize = New System.Drawing.Size(5, 3)
        Me.cmdDelete.Enabled = False
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.SmallImage = CType(resources.GetObject("cmdDelete.SmallImage"), System.Drawing.Image)
        Me.cmdDelete.Style = System.Windows.Forms.RibbonButtonStyle.Normal
        Me.cmdDelete.Tag = Nothing
        Me.cmdDelete.Text = "Delete"
        Me.cmdDelete.ToolTip = Nothing
        Me.cmdDelete.ToolTipTitle = Nothing
        Me.cmdDelete.Value = Nothing
        '
        'RPAction
        '
        Me.RPAction.ButtonMoreEnabled = False
        Me.RPAction.ButtonMoreVisible = False
        Me.RPAction.Items.Add(Me.cmdUndo)
        Me.RPAction.Items.Add(Me.cmdSave)
        Me.RPAction.Tag = Nothing
        Me.RPAction.Text = ""
        '
        'cmdUndo
        '
        Me.cmdUndo.AltKey = Nothing
        Me.cmdUndo.CheckedGroup = Nothing
        Me.cmdUndo.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down
        Me.cmdUndo.DropDownArrowSize = New System.Drawing.Size(5, 3)
        Me.cmdUndo.Enabled = False
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.SmallImage = CType(resources.GetObject("cmdUndo.SmallImage"), System.Drawing.Image)
        Me.cmdUndo.Style = System.Windows.Forms.RibbonButtonStyle.Normal
        Me.cmdUndo.Tag = Nothing
        Me.cmdUndo.Text = "Undo"
        Me.cmdUndo.ToolTip = Nothing
        Me.cmdUndo.ToolTipTitle = Nothing
        Me.cmdUndo.Value = Nothing
        '
        'cmdSave
        '
        Me.cmdSave.AltKey = Nothing
        Me.cmdSave.CheckedGroup = Nothing
        Me.cmdSave.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down
        Me.cmdSave.DropDownArrowSize = New System.Drawing.Size(5, 3)
        Me.cmdSave.Enabled = False
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.SmallImage = CType(resources.GetObject("cmdSave.SmallImage"), System.Drawing.Image)
        Me.cmdSave.Style = System.Windows.Forms.RibbonButtonStyle.Normal
        Me.cmdSave.Tag = Nothing
        Me.cmdSave.Text = "Save"
        Me.cmdSave.ToolTip = "Save Record..."
        Me.cmdSave.ToolTipTitle = Nothing
        Me.cmdSave.Value = Nothing
        '
        'RibbonPanel3
        '
        Me.RibbonPanel3.ButtonMoreEnabled = False
        Me.RibbonPanel3.ButtonMoreVisible = False
        Me.RibbonPanel3.Items.Add(Me.cmdFirst)
        Me.RibbonPanel3.Items.Add(Me.cmdPrevious)
        Me.RibbonPanel3.Items.Add(Me.cmdNext)
        Me.RibbonPanel3.Items.Add(Me.cmdLast)
        Me.RibbonPanel3.Tag = Nothing
        Me.RibbonPanel3.Text = ""
        '
        'cmdFirst
        '
        Me.cmdFirst.AltKey = Nothing
        Me.cmdFirst.CheckedGroup = Nothing
        Me.cmdFirst.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down
        Me.cmdFirst.DropDownArrowSize = New System.Drawing.Size(5, 3)
        Me.cmdFirst.Enabled = False
        Me.cmdFirst.Image = CType(resources.GetObject("cmdFirst.Image"), System.Drawing.Image)
        Me.cmdFirst.SmallImage = CType(resources.GetObject("cmdFirst.SmallImage"), System.Drawing.Image)
        Me.cmdFirst.Style = System.Windows.Forms.RibbonButtonStyle.Normal
        Me.cmdFirst.Tag = Nothing
        Me.cmdFirst.Text = "First"
        Me.cmdFirst.ToolTip = Nothing
        Me.cmdFirst.ToolTipTitle = Nothing
        Me.cmdFirst.Value = Nothing
        '
        'cmdPrevious
        '
        Me.cmdPrevious.AltKey = Nothing
        Me.cmdPrevious.CheckedGroup = Nothing
        Me.cmdPrevious.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down
        Me.cmdPrevious.DropDownArrowSize = New System.Drawing.Size(5, 3)
        Me.cmdPrevious.Enabled = False
        Me.cmdPrevious.Image = CType(resources.GetObject("cmdPrevious.Image"), System.Drawing.Image)
        Me.cmdPrevious.SmallImage = CType(resources.GetObject("cmdPrevious.SmallImage"), System.Drawing.Image)
        Me.cmdPrevious.Style = System.Windows.Forms.RibbonButtonStyle.Normal
        Me.cmdPrevious.Tag = Nothing
        Me.cmdPrevious.Text = "Previous"
        Me.cmdPrevious.ToolTip = Nothing
        Me.cmdPrevious.ToolTipTitle = Nothing
        Me.cmdPrevious.Value = Nothing
        '
        'cmdNext
        '
        Me.cmdNext.AltKey = Nothing
        Me.cmdNext.CheckedGroup = Nothing
        Me.cmdNext.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down
        Me.cmdNext.DropDownArrowSize = New System.Drawing.Size(5, 3)
        Me.cmdNext.Enabled = False
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.SmallImage = CType(resources.GetObject("cmdNext.SmallImage"), System.Drawing.Image)
        Me.cmdNext.Style = System.Windows.Forms.RibbonButtonStyle.Normal
        Me.cmdNext.Tag = Nothing
        Me.cmdNext.Text = "Next"
        Me.cmdNext.ToolTip = Nothing
        Me.cmdNext.ToolTipTitle = Nothing
        Me.cmdNext.Value = Nothing
        '
        'cmdLast
        '
        Me.cmdLast.AltKey = Nothing
        Me.cmdLast.CheckedGroup = Nothing
        Me.cmdLast.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down
        Me.cmdLast.DropDownArrowSize = New System.Drawing.Size(5, 3)
        Me.cmdLast.Enabled = False
        Me.cmdLast.Image = CType(resources.GetObject("cmdLast.Image"), System.Drawing.Image)
        Me.cmdLast.SmallImage = CType(resources.GetObject("cmdLast.SmallImage"), System.Drawing.Image)
        Me.cmdLast.Style = System.Windows.Forms.RibbonButtonStyle.Normal
        Me.cmdLast.Tag = Nothing
        Me.cmdLast.Text = "Last"
        Me.cmdLast.ToolTip = Nothing
        Me.cmdLast.ToolTipTitle = Nothing
        Me.cmdLast.Value = Nothing
        '
        'RibbonPanel2
        '
        Me.RibbonPanel2.ButtonMoreEnabled = False
        Me.RibbonPanel2.ButtonMoreVisible = False
        Me.RibbonPanel2.Items.Add(Me.cmdFind)
        Me.RibbonPanel2.Items.Add(Me.cmdPrint)
        Me.RibbonPanel2.Tag = Nothing
        Me.RibbonPanel2.Text = ""
        '
        'cmdFind
        '
        Me.cmdFind.AltKey = Nothing
        Me.cmdFind.CheckedGroup = Nothing
        Me.cmdFind.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down
        Me.cmdFind.DropDownArrowSize = New System.Drawing.Size(5, 3)
        Me.cmdFind.Enabled = False
        Me.cmdFind.Image = CType(resources.GetObject("cmdFind.Image"), System.Drawing.Image)
        Me.cmdFind.SmallImage = CType(resources.GetObject("cmdFind.SmallImage"), System.Drawing.Image)
        Me.cmdFind.Style = System.Windows.Forms.RibbonButtonStyle.Normal
        Me.cmdFind.Tag = Nothing
        Me.cmdFind.Text = "Find"
        Me.cmdFind.ToolTip = Nothing
        Me.cmdFind.ToolTipTitle = Nothing
        Me.cmdFind.Value = Nothing
        '
        'cmdPrint
        '
        Me.cmdPrint.AltKey = Nothing
        Me.cmdPrint.CheckedGroup = Nothing
        Me.cmdPrint.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down
        Me.cmdPrint.DropDownArrowSize = New System.Drawing.Size(5, 3)
        Me.cmdPrint.Enabled = False
        Me.cmdPrint.Image = CType(resources.GetObject("cmdPrint.Image"), System.Drawing.Image)
        Me.cmdPrint.SmallImage = CType(resources.GetObject("cmdPrint.SmallImage"), System.Drawing.Image)
        Me.cmdPrint.Style = System.Windows.Forms.RibbonButtonStyle.Normal
        Me.cmdPrint.Tag = Nothing
        Me.cmdPrint.Text = "Print"
        Me.cmdPrint.ToolTip = Nothing
        Me.cmdPrint.ToolTipTitle = Nothing
        Me.cmdPrint.Value = Nothing
        '
        'RPExit
        '
        Me.RPExit.ButtonMoreEnabled = False
        Me.RPExit.Items.Add(Me.cmdExitForm)
        Me.RPExit.Tag = Nothing
        Me.RPExit.Text = ""
        '
        'cmdExitForm
        '
        Me.cmdExitForm.AltKey = Nothing
        Me.cmdExitForm.CheckedGroup = Nothing
        Me.cmdExitForm.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down
        Me.cmdExitForm.DropDownArrowSize = New System.Drawing.Size(5, 3)
        Me.cmdExitForm.Enabled = False
        Me.cmdExitForm.Image = CType(resources.GetObject("cmdExitForm.Image"), System.Drawing.Image)
        Me.cmdExitForm.SmallImage = CType(resources.GetObject("cmdExitForm.SmallImage"), System.Drawing.Image)
        Me.cmdExitForm.Style = System.Windows.Forms.RibbonButtonStyle.Normal
        Me.cmdExitForm.Tag = Nothing
        Me.cmdExitForm.Text = "Exit"
        Me.cmdExitForm.ToolTip = Nothing
        Me.cmdExitForm.ToolTipTitle = Nothing
        Me.cmdExitForm.Value = Nothing
        '
        'mnuMain
        '
        Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.SetupToolStripMenuItem, Me.TransactionToolStripMenuItem, Me.ReportsToolStripMenuItem})
        Me.mnuMain.Location = New System.Drawing.Point(0, 0)
        Me.mnuMain.Name = "mnuMain"
        Me.mnuMain.Size = New System.Drawing.Size(1020, 24)
        Me.mnuMain.TabIndex = 2
        Me.mnuMain.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChangePasswordToolStripMenuItem, Me.ToolStripSeparator1, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ChangePasswordToolStripMenuItem
        '
        Me.ChangePasswordToolStripMenuItem.Name = "ChangePasswordToolStripMenuItem"
        Me.ChangePasswordToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ChangePasswordToolStripMenuItem.Text = "Change Password"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(165, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'SetupToolStripMenuItem
        '
        Me.SetupToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VendorToolStripMenuItem, Me.ToolStripMenuItem2, Me.BrandToolStripMenuItem, Me.TyreToolStripMenuItem, Me.PatternToolStripMenuItem, Me.ToolStripSeparator2, Me.WarehouseToolStripMenuItem, Me.CustomerToolStripMenuItem, Me.ToolStripSeparator3, Me.BanksToolStripMenuItem})
        Me.SetupToolStripMenuItem.Name = "SetupToolStripMenuItem"
        Me.SetupToolStripMenuItem.Size = New System.Drawing.Size(49, 20)
        Me.SetupToolStripMenuItem.Text = "Setup"
        '
        'BrandToolStripMenuItem
        '
        Me.BrandToolStripMenuItem.Name = "BrandToolStripMenuItem"
        Me.BrandToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.BrandToolStripMenuItem.Text = "Brand"
        '
        'TyreToolStripMenuItem
        '
        Me.TyreToolStripMenuItem.Name = "TyreToolStripMenuItem"
        Me.TyreToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.TyreToolStripMenuItem.Text = "Size"
        '
        'PatternToolStripMenuItem
        '
        Me.PatternToolStripMenuItem.Name = "PatternToolStripMenuItem"
        Me.PatternToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.PatternToolStripMenuItem.Text = "Pattern"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(171, 6)
        '
        'VendorToolStripMenuItem
        '
        Me.VendorToolStripMenuItem.Name = "VendorToolStripMenuItem"
        Me.VendorToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.VendorToolStripMenuItem.Text = "Vendor / Customer"
        '
        'CustomerToolStripMenuItem
        '
        Me.CustomerToolStripMenuItem.Name = "CustomerToolStripMenuItem"
        Me.CustomerToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.CustomerToolStripMenuItem.Text = "Stock Opeing Qty."
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(171, 6)
        '
        'BanksToolStripMenuItem
        '
        Me.BanksToolStripMenuItem.Name = "BanksToolStripMenuItem"
        Me.BanksToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.BanksToolStripMenuItem.Text = "Banks"
        '
        'WarehouseToolStripMenuItem
        '
        Me.WarehouseToolStripMenuItem.Name = "WarehouseToolStripMenuItem"
        Me.WarehouseToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.WarehouseToolStripMenuItem.Text = "Warehouse"
        '
        'TransactionToolStripMenuItem
        '
        Me.TransactionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PurcahseOrderToolStripMenuItem, Me.SalesInvoiceToolStripMenuItem, Me.ToolStripMenuItem1, Me.PaymentToolStripMenuItem, Me.POReceivedToolStripMenuItem, Me.ToolStripMenuItem3})
        Me.TransactionToolStripMenuItem.Name = "TransactionToolStripMenuItem"
        Me.TransactionToolStripMenuItem.Size = New System.Drawing.Size(85, 20)
        Me.TransactionToolStripMenuItem.Text = "Transactions"
        '
        'PurcahseOrderToolStripMenuItem
        '
        Me.PurcahseOrderToolStripMenuItem.Name = "PurcahseOrderToolStripMenuItem"
        Me.PurcahseOrderToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.PurcahseOrderToolStripMenuItem.Text = "Purcahse Order"
        '
        'SalesInvoiceToolStripMenuItem
        '
        Me.SalesInvoiceToolStripMenuItem.Name = "SalesInvoiceToolStripMenuItem"
        Me.SalesInvoiceToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.SalesInvoiceToolStripMenuItem.Text = "Sales Invoice"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(152, 6)
        '
        'PaymentToolStripMenuItem
        '
        Me.PaymentToolStripMenuItem.Name = "PaymentToolStripMenuItem"
        Me.PaymentToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.PaymentToolStripMenuItem.Text = "Payment"
        '
        'POReceivedToolStripMenuItem
        '
        Me.POReceivedToolStripMenuItem.Name = "POReceivedToolStripMenuItem"
        Me.POReceivedToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.POReceivedToolStripMenuItem.Text = "PO Received"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(171, 6)
        '
        'ReportsToolStripMenuItem
        '
        Me.ReportsToolStripMenuItem.Name = "ReportsToolStripMenuItem"
        Me.ReportsToolStripMenuItem.Size = New System.Drawing.Size(59, 20)
        Me.ReportsToolStripMenuItem.Text = "Reports"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(152, 6)
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1020, 407)
        Me.Controls.Add(Me.Ribbon1)
        Me.Controls.Add(Me.mnuMain)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.mnuMain
        Me.Name = "frmMain"
        Me.Text = "Form2"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnuMain.ResumeLayout(False)
        Me.mnuMain.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Ribbon1 As System.Windows.Forms.Ribbon
    Friend WithEvents RibbonTab1 As System.Windows.Forms.RibbonTab
    Friend WithEvents RibbonPanel1 As System.Windows.Forms.RibbonPanel
    Friend WithEvents cmdNew As System.Windows.Forms.RibbonButton
    Friend WithEvents cmdEdit As System.Windows.Forms.RibbonButton
    Friend WithEvents mnuMain As System.Windows.Forms.MenuStrip
    Friend WithEvents TransactionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurcahseOrderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RPAction As System.Windows.Forms.RibbonPanel
    Friend WithEvents RibbonPanel3 As System.Windows.Forms.RibbonPanel
    Friend WithEvents cmdFirst As System.Windows.Forms.RibbonButton
    Friend WithEvents cmdPrevious As System.Windows.Forms.RibbonButton
    Friend WithEvents cmdNext As System.Windows.Forms.RibbonButton
    Friend WithEvents cmdLast As System.Windows.Forms.RibbonButton
    Friend WithEvents RibbonButton5 As System.Windows.Forms.RibbonButton
    Friend WithEvents cmdDelete As System.Windows.Forms.RibbonButton
    Friend WithEvents cmdUndo As System.Windows.Forms.RibbonButton
    Friend WithEvents cmdSave As System.Windows.Forms.RibbonButton
    Friend WithEvents RibbonPanel2 As System.Windows.Forms.RibbonPanel
    Friend WithEvents cmdFind As System.Windows.Forms.RibbonButton
    Friend WithEvents cmdPrint As System.Windows.Forms.RibbonButton
    Friend WithEvents RPExit As System.Windows.Forms.RibbonPanel
    Friend WithEvents cmdExitForm As System.Windows.Forms.RibbonButton
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChangePasswordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BrandToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TyreToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PatternToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents VendorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BanksToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WarehouseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalesInvoiceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PaymentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents POReceivedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
