﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVendor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grpMain = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtOBalance_ReqN = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbType = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtCompnay_Req = New System.Windows.Forms.TextBox()
        Me.lblDyeingCode = New System.Windows.Forms.Label()
        Me.lblPrimaryKey = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSearchID = New System.Windows.Forms.TextBox()
        Me.btnFind = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.grpMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpMain
        '
        Me.grpMain.BackColor = System.Drawing.SystemColors.Control
        Me.grpMain.Controls.Add(Me.Label7)
        Me.grpMain.Controls.Add(Me.txtOBalance_ReqN)
        Me.grpMain.Controls.Add(Me.Label6)
        Me.grpMain.Controls.Add(Me.cmbType)
        Me.grpMain.Controls.Add(Me.Label5)
        Me.grpMain.Controls.Add(Me.Label4)
        Me.grpMain.Controls.Add(Me.Label3)
        Me.grpMain.Controls.Add(Me.TextBox2)
        Me.grpMain.Controls.Add(Me.TextBox1)
        Me.grpMain.Controls.Add(Me.txtAddress)
        Me.grpMain.Controls.Add(Me.txtCompnay_Req)
        Me.grpMain.Controls.Add(Me.lblDyeingCode)
        Me.grpMain.Controls.Add(Me.lblPrimaryKey)
        Me.grpMain.Controls.Add(Me.Label1)
        Me.grpMain.Enabled = False
        Me.grpMain.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpMain.ForeColor = System.Drawing.SystemColors.ControlText
        Me.grpMain.Location = New System.Drawing.Point(8, 35)
        Me.grpMain.Name = "grpMain"
        Me.grpMain.Size = New System.Drawing.Size(500, 224)
        Me.grpMain.TabIndex = 33
        Me.grpMain.TabStop = False
        Me.grpMain.Text = "Brand Information"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(243, 183)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 13)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Balance :"
        '
        'txtOBalance_ReqN
        '
        Me.txtOBalance_ReqN.AccessibleDescription = "Balance "
        Me.txtOBalance_ReqN.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOBalance_ReqN.Location = New System.Drawing.Point(309, 180)
        Me.txtOBalance_ReqN.Name = "txtOBalance_ReqN"
        Me.txtOBalance_ReqN.Size = New System.Drawing.Size(143, 21)
        Me.txtOBalance_ReqN.TabIndex = 5
        Me.txtOBalance_ReqN.Tag = "OBalance"
        Me.txtOBalance_ReqN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(268, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 13)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Type :"
        '
        'cmbType
        '
        Me.cmbType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbType.FormattingEnabled = True
        Me.cmbType.Location = New System.Drawing.Point(312, 22)
        Me.cmbType.Name = "cmbType"
        Me.cmbType.Size = New System.Drawing.Size(140, 21)
        Me.cmbType.TabIndex = 0
        Me.cmbType.Tag = "TypeID"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(8, 157)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(88, 13)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Contact Person :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(33, 130)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 13)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "Contact # :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(43, 74)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "Address :"
        '
        'TextBox2
        '
        Me.TextBox2.AccessibleDescription = "Compnay"
        Me.TextBox2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(96, 153)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(356, 21)
        Me.TextBox2.TabIndex = 4
        Me.TextBox2.Tag = "ContactPerson"
        '
        'TextBox1
        '
        Me.TextBox1.AccessibleDescription = "Compnay"
        Me.TextBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(96, 127)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(356, 21)
        Me.TextBox1.TabIndex = 3
        Me.TextBox1.Tag = "ContactNo"
        '
        'txtAddress
        '
        Me.txtAddress.AccessibleDescription = ""
        Me.txtAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(96, 74)
        Me.txtAddress.MaxLength = 500
        Me.txtAddress.Multiline = True
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAddress.Size = New System.Drawing.Size(356, 46)
        Me.txtAddress.TabIndex = 2
        Me.txtAddress.Tag = "Address"
        '
        'txtCompnay_Req
        '
        Me.txtCompnay_Req.AccessibleDescription = "Compnay"
        Me.txtCompnay_Req.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompnay_Req.Location = New System.Drawing.Point(96, 47)
        Me.txtCompnay_Req.Name = "txtCompnay_Req"
        Me.txtCompnay_Req.Size = New System.Drawing.Size(356, 21)
        Me.txtCompnay_Req.TabIndex = 1
        Me.txtCompnay_Req.Tag = "Compnay"
        '
        'lblDyeingCode
        '
        Me.lblDyeingCode.AutoSize = True
        Me.lblDyeingCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDyeingCode.Location = New System.Drawing.Point(37, 51)
        Me.lblDyeingCode.Name = "lblDyeingCode"
        Me.lblDyeingCode.Size = New System.Drawing.Size(59, 13)
        Me.lblDyeingCode.TabIndex = 16
        Me.lblDyeingCode.Text = "Compnay :"
        '
        'lblPrimaryKey
        '
        Me.lblPrimaryKey.AutoSize = True
        Me.lblPrimaryKey.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrimaryKey.ForeColor = System.Drawing.Color.DarkRed
        Me.lblPrimaryKey.Location = New System.Drawing.Point(96, 26)
        Me.lblPrimaryKey.Name = "lblPrimaryKey"
        Me.lblPrimaryKey.Size = New System.Drawing.Size(35, 13)
        Me.lblPrimaryKey.TabIndex = 1
        Me.lblPrimaryKey.Tag = "VendorID"
        Me.lblPrimaryKey.Text = "9999"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(71, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(25, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "ID :"
        '
        'txtSearchID
        '
        Me.txtSearchID.Location = New System.Drawing.Point(304, 8)
        Me.txtSearchID.Name = "txtSearchID"
        Me.txtSearchID.Size = New System.Drawing.Size(90, 20)
        Me.txtSearchID.TabIndex = 0
        Me.txtSearchID.Tag = ""
        '
        'btnFind
        '
        Me.btnFind.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFind.Location = New System.Drawing.Point(396, 8)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(112, 22)
        Me.btnFind.TabIndex = 1
        Me.btnFind.Text = "&Find By Lookup"
        Me.btnFind.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Blue
        Me.Label2.Location = New System.Drawing.Point(225, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "Find By ID :"
        '
        'frmVendor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(514, 271)
        Me.Controls.Add(Me.grpMain)
        Me.Controls.Add(Me.txtSearchID)
        Me.Controls.Add(Me.btnFind)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmVendor"
        Me.Text = "Vendor Information"
        Me.grpMain.ResumeLayout(False)
        Me.grpMain.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents grpMain As System.Windows.Forms.GroupBox
    Friend WithEvents txtCompnay_Req As System.Windows.Forms.TextBox
    Friend WithEvents lblDyeingCode As System.Windows.Forms.Label
    Friend WithEvents lblPrimaryKey As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSearchID As System.Windows.Forms.TextBox
    Friend WithEvents btnFind As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbType As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtOBalance_ReqN As System.Windows.Forms.TextBox
End Class
