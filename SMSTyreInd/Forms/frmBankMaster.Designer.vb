﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBankMaster
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grpMain = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtBalance_ReqN = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtBankCode_Req = New System.Windows.Forms.TextBox()
        Me.txtAccountNo_Req = New System.Windows.Forms.TextBox()
        Me.txtBranch_Req = New System.Windows.Forms.TextBox()
        Me.txtBank_Req = New System.Windows.Forms.TextBox()
        Me.lblDyeingCode = New System.Windows.Forms.Label()
        Me.lblPrimaryKey = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSearchID = New System.Windows.Forms.TextBox()
        Me.btnFind = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtRemarks = New System.Windows.Forms.TextBox()
        Me.grpMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpMain
        '
        Me.grpMain.BackColor = System.Drawing.SystemColors.Control
        Me.grpMain.Controls.Add(Me.Label6)
        Me.grpMain.Controls.Add(Me.txtRemarks)
        Me.grpMain.Controls.Add(Me.Label7)
        Me.grpMain.Controls.Add(Me.txtBalance_ReqN)
        Me.grpMain.Controls.Add(Me.Label5)
        Me.grpMain.Controls.Add(Me.Label4)
        Me.grpMain.Controls.Add(Me.Label3)
        Me.grpMain.Controls.Add(Me.txtBankCode_Req)
        Me.grpMain.Controls.Add(Me.txtAccountNo_Req)
        Me.grpMain.Controls.Add(Me.txtBranch_Req)
        Me.grpMain.Controls.Add(Me.txtBank_Req)
        Me.grpMain.Controls.Add(Me.lblDyeingCode)
        Me.grpMain.Controls.Add(Me.lblPrimaryKey)
        Me.grpMain.Controls.Add(Me.Label1)
        Me.grpMain.Enabled = False
        Me.grpMain.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpMain.ForeColor = System.Drawing.SystemColors.ControlText
        Me.grpMain.Location = New System.Drawing.Point(8, 35)
        Me.grpMain.Name = "grpMain"
        Me.grpMain.Size = New System.Drawing.Size(500, 239)
        Me.grpMain.TabIndex = 33
        Me.grpMain.TabStop = False
        Me.grpMain.Text = "Brand Information"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(252, 209)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 13)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Balance :"
        '
        'txtBalance_ReqN
        '
        Me.txtBalance_ReqN.AccessibleDescription = "Balance "
        Me.txtBalance_ReqN.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBalance_ReqN.Location = New System.Drawing.Point(309, 206)
        Me.txtBalance_ReqN.Name = "txtBalance_ReqN"
        Me.txtBalance_ReqN.Size = New System.Drawing.Size(143, 21)
        Me.txtBalance_ReqN.TabIndex = 5
        Me.txtBalance_ReqN.Tag = "Balance"
        Me.txtBalance_ReqN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(238, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(65, 13)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Bank Code :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(32, 130)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "Account # :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(0, 74)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 13)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "Branch / Address :"
        '
        'txtBankCode_Req
        '
        Me.txtBankCode_Req.AccessibleDescription = "Bank Code"
        Me.txtBankCode_Req.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBankCode_Req.Location = New System.Drawing.Point(309, 22)
        Me.txtBankCode_Req.Name = "txtBankCode_Req"
        Me.txtBankCode_Req.Size = New System.Drawing.Size(143, 21)
        Me.txtBankCode_Req.TabIndex = 0
        Me.txtBankCode_Req.Tag = "BankCode"
        '
        'txtAccountNo_Req
        '
        Me.txtAccountNo_Req.AccessibleDescription = "Account No"
        Me.txtAccountNo_Req.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccountNo_Req.Location = New System.Drawing.Point(96, 127)
        Me.txtAccountNo_Req.Name = "txtAccountNo_Req"
        Me.txtAccountNo_Req.Size = New System.Drawing.Size(356, 21)
        Me.txtAccountNo_Req.TabIndex = 3
        Me.txtAccountNo_Req.Tag = "AccountNo"
        '
        'txtBranch_Req
        '
        Me.txtBranch_Req.AccessibleDescription = "Branch & Address"
        Me.txtBranch_Req.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBranch_Req.Location = New System.Drawing.Point(96, 74)
        Me.txtBranch_Req.MaxLength = 500
        Me.txtBranch_Req.Multiline = True
        Me.txtBranch_Req.Name = "txtBranch_Req"
        Me.txtBranch_Req.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtBranch_Req.Size = New System.Drawing.Size(356, 46)
        Me.txtBranch_Req.TabIndex = 2
        Me.txtBranch_Req.Tag = "Branch"
        '
        'txtBank_Req
        '
        Me.txtBank_Req.AccessibleDescription = "Bank Name"
        Me.txtBank_Req.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBank_Req.Location = New System.Drawing.Point(96, 47)
        Me.txtBank_Req.Name = "txtBank_Req"
        Me.txtBank_Req.Size = New System.Drawing.Size(356, 21)
        Me.txtBank_Req.TabIndex = 1
        Me.txtBank_Req.Tag = "Bank"
        '
        'lblDyeingCode
        '
        Me.lblDyeingCode.AutoSize = True
        Me.lblDyeingCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDyeingCode.Location = New System.Drawing.Point(59, 51)
        Me.lblDyeingCode.Name = "lblDyeingCode"
        Me.lblDyeingCode.Size = New System.Drawing.Size(37, 13)
        Me.lblDyeingCode.TabIndex = 16
        Me.lblDyeingCode.Text = "Bank :"
        '
        'lblPrimaryKey
        '
        Me.lblPrimaryKey.AutoSize = True
        Me.lblPrimaryKey.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrimaryKey.ForeColor = System.Drawing.Color.DarkRed
        Me.lblPrimaryKey.Location = New System.Drawing.Point(96, 26)
        Me.lblPrimaryKey.Name = "lblPrimaryKey"
        Me.lblPrimaryKey.Size = New System.Drawing.Size(35, 13)
        Me.lblPrimaryKey.TabIndex = 1
        Me.lblPrimaryKey.Tag = "BankID"
        Me.lblPrimaryKey.Text = "9999"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(71, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(25, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "ID :"
        '
        'txtSearchID
        '
        Me.txtSearchID.Location = New System.Drawing.Point(304, 8)
        Me.txtSearchID.Name = "txtSearchID"
        Me.txtSearchID.Size = New System.Drawing.Size(90, 20)
        Me.txtSearchID.TabIndex = 0
        Me.txtSearchID.Tag = ""
        '
        'btnFind
        '
        Me.btnFind.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFind.Location = New System.Drawing.Point(396, 8)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(112, 22)
        Me.btnFind.TabIndex = 1
        Me.btnFind.Text = "&Find By Lookup"
        Me.btnFind.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Blue
        Me.Label2.Location = New System.Drawing.Point(225, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "Find By ID :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(41, 154)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 31
        Me.Label6.Text = "Remarks :"
        '
        'txtRemarks
        '
        Me.txtRemarks.AccessibleDescription = ""
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.Location = New System.Drawing.Point(96, 154)
        Me.txtRemarks.MaxLength = 500
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(356, 46)
        Me.txtRemarks.TabIndex = 4
        Me.txtRemarks.Tag = "Remarks"
        '
        'frmBankMaster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(514, 279)
        Me.Controls.Add(Me.grpMain)
        Me.Controls.Add(Me.txtSearchID)
        Me.Controls.Add(Me.btnFind)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmBankMaster"
        Me.Text = "`"
        Me.grpMain.ResumeLayout(False)
        Me.grpMain.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents grpMain As System.Windows.Forms.GroupBox
    Friend WithEvents txtBank_Req As System.Windows.Forms.TextBox
    Friend WithEvents lblDyeingCode As System.Windows.Forms.Label
    Friend WithEvents lblPrimaryKey As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSearchID As System.Windows.Forms.TextBox
    Friend WithEvents btnFind As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtBankCode_Req As System.Windows.Forms.TextBox
    Friend WithEvents txtAccountNo_Req As System.Windows.Forms.TextBox
    Friend WithEvents txtBranch_Req As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtBalance_ReqN As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As System.Windows.Forms.TextBox
End Class
