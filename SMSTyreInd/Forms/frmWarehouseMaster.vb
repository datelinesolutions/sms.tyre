﻿Public Class frmWarehouseMaster

#Region "Form local variables"
    Dim objFormClass As New clsWarehouse
    Dim FormDataTable As New DataTable
    Dim l_CurrentRowID As Integer = -1

#End Region

#Region "Form's Events"
    Private Sub frmWarehouseMaster_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        'Set value for toolbar action buttons
        mdlV_CurrentForm_TotalRecords = FormDataTable.Rows.Count
        Call mdl_ActivedEventToolbar(Me.ParentForm)
    End Sub

    Private Sub frmBrand_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim xDataTable As New DataTable

        Me.Text = objFormClass.cp_formTitle & ""
        Me.grpMain.Text = objFormClass.cp_formTitle & " Information"
        '
        ' Fill The Form's Comboboxes
        '
        'xDataTable = objFormClass.getDataTable("SELECT * FROM vw_Inline_ClientType ORDER BY 1")
        'Call mdl_FillFormCombobox(xDataTable, cmbType, "TypeID", "TypeName")
        '
        ''
        ' END OF COMBOBOXES FILLING
        '
        FormDataTable = objFormClass.getDataTable()
        If FormDataTable.Rows.Count <> 0 Then
            l_CurrentRowID = 0
            Call GetFormData(FormDataTable, l_CurrentRowID)
        End If
        '
        'Set value for toolbar action buttons
        mdlV_CurrentForm_TotalRecords = FormDataTable.Rows.Count
    End Sub
    Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click
        Call FindRecordByLookup()
    End Sub
    Private Sub txtSearchID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSearchID.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            If txtSearchID.Text <> "" Then Call FindData(txtSearchID.Text)
        End If
        Call KeyPressNumeric(e)
    End Sub
#End Region

#Region "Form Action Routines"
    Private Sub GetFormData(ByVal l_DataTable As DataTable, ByVal l_RecordIndex As Integer)
        l_CurrentRowID = l_RecordIndex
        Call mdl_PopulateFormControls(l_DataTable, l_RecordIndex, Me.grpMain)
    End Sub
    Public Sub FirstRecord()
        If l_CurrentRowID = -1 Or l_CurrentRowID = 0 Then Exit Sub
        l_CurrentRowID = 0
        Call GetFormData(FormDataTable, l_CurrentRowID)
    End Sub
    Public Sub PreviousRecord()
        If l_CurrentRowID = 0 Or l_CurrentRowID = -1 Then Exit Sub
        l_CurrentRowID -= 1
        Call GetFormData(FormDataTable, l_CurrentRowID)
    End Sub
    Public Sub NextRecord()
        If l_CurrentRowID = FormDataTable.Rows.Count - 1 Or l_CurrentRowID = -1 Then Exit Sub
        l_CurrentRowID += 1
        Call GetFormData(FormDataTable, l_CurrentRowID)
    End Sub
    Public Sub LastRecord()
        If l_CurrentRowID = FormDataTable.Rows.Count - 1 Or l_CurrentRowID = -1 Then Exit Sub
        l_CurrentRowID = FormDataTable.Rows.Count - 1
        Call GetFormData(FormDataTable, l_CurrentRowID)
    End Sub
    Public Sub NewRecord()
        Call mdl_EmptyFormControls(Me.grpMain)
        grpMain.Enabled = True
        btnFind.Enabled = False
        txtSearchID.Enabled = False
        'lblPrimaryKey.Text = ""
        txtWarehouse_Req.Focus()
    End Sub
    Public Sub EditRecord()
        grpMain.Enabled = True
        btnFind.Enabled = False
        txtSearchID.Enabled = False
    End Sub
    Public Sub UndoAction()
        grpMain.Enabled = False
        btnFind.Enabled = True
        txtSearchID.Enabled = True
        If l_CurrentRowID <> -1 Then Call GetFormData(FormDataTable, l_CurrentRowID)
    End Sub
    Public Sub FindRecordByLookup()
        Dim l_objfrmFind As New frmFind
        Dim l_returnValue As String
        '
        '
        '
        Try
            mdl_FindRecordByLookup_class(l_objfrmFind, objFormClass.cp_formTitle, objFormClass.cp_searchQuery)
            l_returnValue = l_objfrmFind.GetFindValue.ToString()
            If l_returnValue <> "" Then Call FindData(l_returnValue)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub FindData(ByVal p_SearchResult As Integer)
        If p_SearchResult = 0 Then Exit Sub
        Dim _rowIndex As Integer = -1

        For i As Integer = 0 To FormDataTable.Rows.Count - 1
            If FormDataTable.Rows(i)(objFormClass.cp_primaryKey) = p_SearchResult Then
                _rowIndex = i
                Exit For
            End If
        Next
        If _rowIndex <> -1 Then
            Call GetFormData(FormDataTable, _rowIndex)
        Else
            MsgBox("Record Not Found", MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
        End If
        txtSearchID.SelectAll()
    End Sub
    Public Sub SaveRecord()
        Dim xArrayList As New ArrayList
        Dim xSQLQuery As String = ""
        '
        ' BEGIN: Forms Input Validation
        '
        If Not mdl_FormValidation(Me.grpMain) Then
            mdlV_isFormValidate = False
            Exit Sub
        Else
            mdlV_isFormValidate = True
        End If
        ' END: Forms Input Validation
        '
        xArrayList = mdl_PopulateDataColumns(Me.grpMain)

        If lblPrimaryKey.Text = "" Then
            If (objFormClass.saveRecorde(FormDataTable, xArrayList)) Then MsgBox("Record Save successfuly", MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
        Else
            If (objFormClass.editRecord(FormDataTable, xArrayList, lblPrimaryKey.Text)) Then MsgBox("Record Update successfuly", MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
        End If
        '
        ' Set ReadOnly Mode
        '
        FormDataTable.Clear()
        FormDataTable = objFormClass.getDataTable() '  Refresh DataTable 
        mdlV_CurrentForm_TotalRecords = FormDataTable.Rows.Count()
        If lblPrimaryKey.Text = "" Then l_CurrentRowID = FormDataTable.Rows.Count() - 1 ' If new record go to last record
        Call UndoAction()
    End Sub
    '
    ' DELETE RECORD FROM TABLE; Set IsDelete Flag True
    '
    Public Sub DeleteRecord()
        If MsgBox("Are you sure you want to delete this record?", MsgBoxStyle.Critical + MsgBoxStyle.YesNo, "WARNING") = MsgBoxResult.Yes Then
            If (objFormClass.deleteRecord(Convert.ToDecimal(lblPrimaryKey.Text))) Then

                '
                ' Set Other/Next Record after deleted
                '
                FormDataTable.Clear()
                FormDataTable = objFormClass.getDataTable() '  Refresh DataTable 
                With FormDataTable
                    If .Rows.Count <> 0 Then
                        If l_CurrentRowID > .Rows.Count - 1 Then l_CurrentRowID -= 1
                        Call GetFormData(FormDataTable, l_CurrentRowID)
                    Else
                        l_CurrentRowID = -1 ' DataTable is Blank
                    End If
                    '
                    'Set value for toolbar action buttons
                    mdlV_CurrentForm_TotalRecords = FormDataTable.Rows.Count
                End With
                MsgBox("Record Delete Successfuly.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
            End If
        End If
    End Sub
#End Region

End Class