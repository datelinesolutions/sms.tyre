﻿Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
'Imports System.Linq
Imports System.Text
Imports System.Windows.Forms

Public Class frmFind
    Inherits Form
    Public Property ds() As DataSet
        Get
            Return m_ds
        End Get
        Set(ByVal value As DataSet)
            m_ds = value
        End Set
    End Property
    Private m_ds As DataSet

    Public Property dv() As DataView
        Get
            Return m_dv
        End Get
        Set(ByVal value As DataView)
            m_dv = value
        End Set
    End Property
    Private m_dv As DataView
    Public Sub New()
        InitializeComponent()
        ds = New DataSet()
        dv = New DataView()
    End Sub

    Dim objDatabase As New clsDatabase
    Dim l_query As String
    Dim l_searchValue As Integer

    Public ReadOnly Property GetFindValue()
        Get
            Return l_searchValue
        End Get
    End Property

    Public WriteOnly Property SetFindQuery()
        Set(ByVal value As Object)
            l_query = value
        End Set
    End Property

    Private Sub frmFind_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ds = objDatabase.getFindFormDataset(l_query)
        'ds = objDatabase.GF_DataSet("SELECT CustomerID As [Customer ID], CustomerName As [Customer Name], CustomerEmail As [Customer Email], CustomerPhone As [Customer Phone], CustomerName+ ' ' + CustomerEmail + ' ' +  CustomerPhone As SearchResult FROM vw_Customer")
        dv.Table = ds.Tables(0)
        'dgvResult.DataSource = dv
        With dgvResult
            .DataSource = dv
            '.Columns(0).HeaderText = "Customer ID"
            .Columns("SearchResult").Visible = False
        End With

    End Sub
    Private Sub txtSearch_TabIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TabIndexChanged

        'If dv Is Nothing Then
        'Else
        '    dv.RowFilter = "CustomerName like '%" + txtSearch.Text + "%'"
        '    DataGridView1.DataSource = dv
        'End If

    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        dv.RowFilter = "SearchResult like '%" + txtSearch.Text + "%'"
        dgvResult.DataSource = dv
    End Sub
    Private Sub dgvResult_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvResult.DoubleClick
        Try

            ' If (dgvResult.ColumnHeaderSelect.Equals(DataGridViewSelectionMode.ColumnHeaderSelect ) Then
            l_searchValue = dgvResult.SelectedRows.Item(0).Cells(0).Value.ToString()
            Me.Visible = False
            Me.Dispose()
            ' End If
        Catch ex As Exception
            MsgBox("Please click on proper cell/row", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
        End Try

    End Sub

    Private Sub dgvResult_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvResult.CellContentClick

    End Sub
End Class