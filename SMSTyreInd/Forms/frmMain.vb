﻿Public Class frmMain

#Region "Form Menus Click Event"
    Private Sub PurcahseOrderToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PurcahseOrderToolStripMenuItem.Click
        mdl_showMDIChild(New frmPurchaseOrder)
    End Sub
    Private Sub BrandToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BrandToolStripMenuItem.Click
        mdl_showMDIChild(New frmBrand)
    End Sub
    Private Sub TyreToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TyreToolStripMenuItem.Click
        mdl_showMDIChild(New frmSize)
    End Sub
    Private Sub PatternToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PatternToolStripMenuItem.Click
        mdl_showMDIChild(New frmPattern)
    End Sub
    Private Sub VendorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VendorToolStripMenuItem.Click
        mdl_showMDIChild(New frmVendor)
    End Sub
    Private Sub BanksToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BanksToolStripMenuItem.Click
        mdl_showMDIChild(New frmBankMaster)
    End Sub
    Private Sub WarehouseToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles WarehouseToolStripMenuItem.Click
        mdl_showMDIChild(New frmWarehouseMaster)
    End Sub
    Private Sub SalesInvoiceToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalesInvoiceToolStripMenuItem.Click

    End Sub
    Private Sub PaymentToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PaymentToolStripMenuItem.Click
        mdl_showMDIChild(New frmPayment)
    End Sub

    Private Sub POReceivedToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles POReceivedToolStripMenuItem.Click

    End Sub
#End Region

#Region "Genral sub routins and procedures"
    Public Sub setToolbarButtonsState()
        Call Me.SetToolbarNavigation("Load")
    End Sub

    Public Sub SetToolbarNavigation(ByVal State As String)
        ' Dim activeChild As Form = Me.ActiveMdiChild

        Select Case State
            Case "First" ' Navigation
                cmdFirst.Enabled = False
                cmdPrevious.Enabled = False
                cmdNext.Enabled = True
                cmdLast.Enabled = True
                Exit Sub
            Case "Next" ' Navigation
                cmdFirst.Enabled = True
                cmdPrevious.Enabled = True
                Exit Sub
            Case "Previous" ' Navigation
                cmdNext.Enabled = True
                cmdLast.Enabled = True
                Exit Sub
            Case "Last" ' Navigation
                cmdFirst.Enabled = True
                cmdPrevious.Enabled = True
                cmdNext.Enabled = False
                cmdLast.Enabled = False
                Exit Sub
            Case Else
                cmdNew.Enabled = False
                cmdEdit.Enabled = False ' Edit
                cmdDelete.Enabled = False ' Del

                cmdUndo.Enabled = False ' Undo
                cmdSave.Enabled = False ' Save

                cmdFirst.Enabled = False
                cmdPrevious.Enabled = False
                cmdNext.Enabled = False
                cmdLast.Enabled = False

                cmdFind.Enabled = False
                cmdPrint.Enabled = False

                'If State.Equals("Start") Then Exit Sub
                Select Case State
                    Case "Load"
                        cmdNew.Enabled = True ' New
                        If mdlV_CurrentForm_TotalRecords <> 0 Then
                            cmdEdit.Enabled = True ' Edit
                            cmdDelete.Enabled = True ' Del

                            'cmdUndo.Enabled = True ' Undo
                            'cmdSave.Enabled = True ' Save

                            If mdlV_CurrentForm_TotalRecords > 1 Then
                                cmdFirst.Enabled = True
                                cmdPrevious.Enabled = True
                                cmdNext.Enabled = True
                                cmdLast.Enabled = True

                                cmdFind.Enabled = True
                            End If
                            cmdPrint.Enabled = True
                        End If
                        cmdExitForm.Enabled = True
                    Case "New"
                        cmdUndo.Enabled = True ' Undo
                        cmdSave.Enabled = True ' Save
                    Case "Save"
                        ' Navigation
                        Call Me.SetToolbarNavigation("Load")
                    Case "Edit"
                        cmdUndo.Enabled = True ' Undo
                        cmdSave.Enabled = True ' Save
                    Case "Undo"
                        ' Navigation
                        Call Me.SetToolbarNavigation("Load")
                    Case "Start"
                        cmdExitForm.Enabled = False
                End Select
        End Select
    End Sub
#End Region

#Region "Toolbar buttons event"
    Private Sub cmdNew_Click(sender As Object, e As EventArgs) Handles cmdNew.Click
        Dim activeChild As Form = Me.ActiveMdiChild
        Dim m As Type = activeChild.[GetType]()

        Dim NewRecord As Reflection.MethodInfo = m.GetMethod("NewRecord")

        If NewRecord IsNot Nothing Then
            NewRecord.Invoke(activeChild, Nothing)
        End If
        '
        ' Set Toolbar Action
        Call Me.SetToolbarNavigation("New")
    End Sub

    Private Sub cmdEdit_Click(sender As Object, e As EventArgs) Handles cmdEdit.Click
        Dim activeChild As Form = Me.ActiveMdiChild
        Dim m As Type = activeChild.[GetType]()

        Dim EditRecord As Reflection.MethodInfo = m.GetMethod("EditRecord")
        If EditRecord IsNot Nothing Then
            EditRecord.Invoke(activeChild, Nothing)
        End If
        ' Set Toolbar Action
        If mdlV_isFormValidate Then Call Me.SetToolbarNavigation("Edit")
    End Sub

    Private Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        Dim activeChild As Form = Me.ActiveMdiChild
        Dim m As Type = activeChild.[GetType]()

        Dim DeleteRecord As Reflection.MethodInfo = m.GetMethod("DeleteRecord")
        If DeleteRecord IsNot Nothing Then
            DeleteRecord.Invoke(activeChild, Nothing)
        End If
        ' Set Toolbar Action
        Call Me.SetToolbarNavigation("Load")
    End Sub

    Private Sub cmdUndo_Click(sender As Object, e As EventArgs) Handles cmdUndo.Click
        Dim activeChild As Form = Me.ActiveMdiChild
        Dim m As Type = activeChild.[GetType]()

        Call Me.SetToolbarNavigation("Load")
        Dim UndoAction As Reflection.MethodInfo = m.GetMethod("UndoAction")
        If UndoAction IsNot Nothing Then
            UndoAction.Invoke(activeChild, Nothing)
        End If
    End Sub

    Private Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        Dim activeChild As Form = Me.ActiveMdiChild
        Dim m As Type = activeChild.[GetType]()
        Dim SaveRecord As Reflection.MethodInfo = m.GetMethod("SaveRecord")

        If SaveRecord IsNot Nothing Then
            SaveRecord.Invoke(activeChild, Nothing)
        End If
        If mdlV_isFormValidate Then Call Me.SetToolbarNavigation("Save")
    End Sub

    Private Sub cmdFirst_Click(sender As Object, e As EventArgs) Handles cmdFirst.Click
        Dim activeChild As Form = Me.ActiveMdiChild
        Dim m As Type = activeChild.[GetType]()

        Dim FirstRecord As Reflection.MethodInfo = m.GetMethod("FirstRecord")
        If FirstRecord IsNot Nothing Then
            FirstRecord.Invoke(activeChild, Nothing)
        End If
        Call Me.SetToolbarNavigation("First")
    End Sub

    Private Sub cmdPrevious_Click(sender As Object, e As EventArgs) Handles cmdPrevious.Click
        Dim activeChild As Form = Me.ActiveMdiChild
        Dim m As Type = activeChild.[GetType]()

        Dim PreviousRecord As Reflection.MethodInfo = m.GetMethod("PreviousRecord")
        If PreviousRecord IsNot Nothing Then
            PreviousRecord.Invoke(activeChild, Nothing)
        End If
        Call Me.SetToolbarNavigation("Previous")
    End Sub

    Private Sub cmdNext_Click(sender As Object, e As EventArgs) Handles cmdNext.Click
        Dim activeChild As Form = Me.ActiveMdiChild
        Dim m As Type = activeChild.[GetType]()

        Dim NextRecord As Reflection.MethodInfo = m.GetMethod("NextRecord")
        If NextRecord IsNot Nothing Then
            NextRecord.Invoke(activeChild, Nothing)
        End If
        Call Me.SetToolbarNavigation("Next")
    End Sub

    Private Sub cmdLast_Click(sender As Object, e As EventArgs) Handles cmdLast.Click
        Dim activeChild As Form = Me.ActiveMdiChild
        Dim m As Type = activeChild.[GetType]()

        Dim LastRecord As Reflection.MethodInfo = m.GetMethod("LastRecord")
        If LastRecord IsNot Nothing Then
            LastRecord.Invoke(activeChild, Nothing)
        End If
        Call Me.SetToolbarNavigation("Last")
    End Sub

    Private Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        Dim activeChild As Form = Me.ActiveMdiChild
        Dim m As Type = activeChild.[GetType]()

        Dim FindRecordByLookup As Reflection.MethodInfo = m.GetMethod("FindRecordByLookup")
        If FindRecordByLookup IsNot Nothing Then
            FindRecordByLookup.Invoke(activeChild, Nothing)
        End If
    End Sub

    Private Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click
        Dim activeChild As Form = Me.ActiveMdiChild
        Dim m As Type = activeChild.[GetType]()

    End Sub

    Private Sub cmdExitForm_Click(sender As Object, e As EventArgs) Handles cmdExitForm.Click
        If MsgBox("Are you sure you want to close Form(s)?", MsgBoxStyle.Critical + MsgBoxStyle.YesNo, "WARNING") = MsgBoxResult.Yes Then
            For Each _form As Form In Me.MdiChildren
                _form.Close()
            Next
            mdlV_CurrentForm_TotalRecords = 0
            Call Me.SetToolbarNavigation("Start")
        End If
    End Sub
#End Region




    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        'If MsgBox("Are You Sure to Quit This Program?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "SMS") = MsgBoxResult.Yes Then

        '    Application.Exit()
        'End If
        Me.Close()
    End Sub

    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.Closing 'Me.FormClosing
        If MsgBox("Are You Sure to Quit This Program?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "SMS") = MsgBoxResult.Yes Then
            Application.Exit()
        Else
            e.Cancel = True
        End If
    End Sub





End Class