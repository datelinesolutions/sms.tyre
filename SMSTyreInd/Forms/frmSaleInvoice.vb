﻿Public Class frmSaleInvoice


#Region "Form local variables"
    Dim objFormClass As New clsSaleInvoice
    Dim FormDataTable, FormDataTableDetail As New DataTable

    Dim l_CurrentRowID As Integer = -1

#End Region

#Region "Form's Events"
    Private Sub frmPurchaseOrder_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        'Set value for toolbar action buttons
        mdlV_CurrentForm_TotalRecords = FormDataTable.Rows.Count
        Call mdl_ActivedEventToolbar(Me.ParentForm)
    End Sub

    Private Sub frmPurchaseOrder_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim xDataTable As New DataTable

        Me.Text = objFormClass.cp_formTitle & " Setup"
        Me.grpMain.Text = objFormClass.cp_formTitle & " Information"
        '*************************************
        '* BEGIN: Fill The Form's Comboboxes *
        '*************************************
        xDataTable = objFormClass.getDataTable("SELECT VendorID,  Compnay FROM vw_VendorMaster WHERE ISNULL(IsDeleted, 0) = 0")
        Call mdl_FillFormCombobox(xDataTable, cmbVendorID, "VendorID", "Compnay")
        '
        '------- GIRD Comboboxes Filling ---------
        '
        'xDataTable = objFormClass.getDataTable("SELECT CAST(BrandID As VARCHAR) As BrandID, BrandName FROM  vw_Brand WHERE ISNULL(isDeleted, 0) = 0")
        xDataTable = objFormClass.getDataTable("SELECT BrandID, BrandName FROM  vw_Brand WHERE ISNULL(isDeleted, 0) = 0")
        Call mdl_FillGridCombo(DGView, 0, xDataTable, "BrandID", "BrandID", "BrandName", "Brand", 150)

        xDataTable = objFormClass.getDataTable("SELECT SizeID, SizeName FROM  vw_Size WHERE ISNULL(isDeleted, 0) = 0")
        Call mdl_FillGridCombo(DGView, 1, xDataTable, "SizeID", "SizeID", "SizeName", "Size", 100)

        'xDataTable = objFormClass.getDataTable("SELECT CAST(PatternID AS VARCHAR) As PatternID, PatternName FROM  vw_Pattern WHERE ISNULL(isDeleted, 0) = 0")
        xDataTable = objFormClass.getDataTable("SELECT PatternID, PatternName FROM  vw_GetPattern")
        Call mdl_FillGridCombo(DGView, 2, xDataTable, "PatternID", "PatternID", "PatternName", "Pattern", 90)

        xDataTable = objFormClass.getDataTable("SELECT * FROM vw_Inline_DiscountRebate ORDER BY 1")
        Call mdl_FillGridCombo(DGView, 5, xDataTable, "GiveUpID", "TypeID", "TypeName", "Dis. /Rebate", 80)
        '***********************************
        '* END: Fill The Form's Comboboxes *
        '***********************************
        FormDataTable = objFormClass.getDataTable()
        If FormDataTable.Rows.Count <> 0 Then
            l_CurrentRowID = 0
            Call GetFormData(FormDataTable, l_CurrentRowID)
        End If
        '
        'Set value for toolbar action buttons
        mdlV_CurrentForm_TotalRecords = FormDataTable.Rows.Count
    End Sub

    Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click
        Call FindRecordByLookup()
    End Sub

    Private Sub txtSearchID_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSearchID.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            If txtSearchID.Text <> "" Then Call FindData(txtSearchID.Text)
        End If
        Call KeyPressNumeric(e)
    End Sub
#End Region

#Region "Form Action Routines"
    Private Sub GetFormData(ByVal l_DataTable As DataTable, ByVal l_RecordIndex As Integer)
        l_CurrentRowID = l_RecordIndex
        FormDataTableDetail = objFormClass.getDataTableDetail(FormDataTable.Rows(l_RecordIndex).Item(objFormClass.cp_primaryKey))
        Call mdl_PopulateFormControls(l_DataTable, l_RecordIndex, Me.grpMain, FormDataTableDetail)
    End Sub
    Public Sub FirstRecord()
        If l_CurrentRowID = -1 Or l_CurrentRowID = 0 Then Exit Sub
        l_CurrentRowID = 0
        Call GetFormData(FormDataTable, l_CurrentRowID)
    End Sub
    Public Sub PreviousRecord()
        If l_CurrentRowID = 0 Or l_CurrentRowID = -1 Then Exit Sub
        l_CurrentRowID -= 1
        Call GetFormData(FormDataTable, l_CurrentRowID)
    End Sub
    Public Sub NextRecord()
        If l_CurrentRowID = FormDataTable.Rows.Count - 1 Or l_CurrentRowID = -1 Then Exit Sub
        l_CurrentRowID += 1
        Call GetFormData(FormDataTable, l_CurrentRowID)
    End Sub
    Public Sub LastRecord()
        If l_CurrentRowID = FormDataTable.Rows.Count - 1 Or l_CurrentRowID = -1 Then Exit Sub
        l_CurrentRowID = FormDataTable.Rows.Count - 1
        Call GetFormData(FormDataTable, l_CurrentRowID)
    End Sub
    Public Sub NewRecord()
        Call mdl_EmptyFormControls(Me.grpMain)
        grpMain.Enabled = True
        btnFind.Enabled = False
        txtSearchID.Enabled = False
        'lblPrimaryKey.Text = ""
        'txtCompnay_Req.Focus()
    End Sub
    Public Sub EditRecord()
        grpMain.Enabled = True
        btnFind.Enabled = False
        txtSearchID.Enabled = False
    End Sub
    Public Sub UndoAction()
        grpMain.Enabled = False
        btnFind.Enabled = True
        txtSearchID.Enabled = True
        If l_CurrentRowID <> -1 Then Call GetFormData(FormDataTable, l_CurrentRowID)
    End Sub
    Public Sub FindRecordByLookup()
        Dim l_objfrmFind As New frmFind
        Dim l_returnValue As String
        '
        '
        '
        Try
            mdl_FindRecordByLookup_class(l_objfrmFind, objFormClass.cp_formTitle, objFormClass.cp_searchQuery)
            l_returnValue = l_objfrmFind.GetFindValue.ToString()
            If l_returnValue <> "" Then Call FindData(l_returnValue)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub FindData(ByVal p_SearchResult As Integer)
        If p_SearchResult = 0 Then Exit Sub
        Dim _rowIndex As Integer = -1

        For i As Integer = 0 To FormDataTable.Rows.Count - 1
            If FormDataTable.Rows(i)(objFormClass.cp_primaryKey) = p_SearchResult Then
                _rowIndex = i
                Exit For
            End If
        Next
        If _rowIndex <> -1 Then
            Call GetFormData(FormDataTable, _rowIndex)
        Else
            MsgBox("Record Not Found", MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
        End If
        txtSearchID.SelectAll()
    End Sub
    Public Sub SaveRecord()
        Dim xArrayList, xArrayList_Details As New ArrayList
        Dim xSQLQuery As String = ""
        '
        ' BEGIN: Forms Input Validation
        '
        If Not mdl_FormValidation(Me.grpMain, DGView) Then
            mdlV_isFormValidate = False
            Exit Sub
        Else
            mdlV_isFormValidate = True
        End If
        ' END: Forms Input Validation
        '
        xArrayList = mdl_PopulateDataColumns(Me.grpMain)
        '
        ' Populate Grid Values in Array list 
        '
        xArrayList_Details = mdl_PopulateDetailsTable(Me.DGView, objFormClass.cp_detail_Fields, objFormClass.cp_primaryKey, IIf(lblPrimaryKey.Text = "", 0, lblPrimaryKey.Text))


        If lblPrimaryKey.Text = "" Then
            If (objFormClass.saveRecorde(FormDataTable, xArrayList, xArrayList_Details)) Then MsgBox("Record Save successfuly", MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
        Else
            If (objFormClass.editRecord(FormDataTable, xArrayList, lblPrimaryKey.Text, xArrayList_Details)) Then MsgBox("Record Update successfuly", MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
        End If
        '
        ' Set ReadOnly Mode
        '
        FormDataTable.Clear()
        FormDataTable = objFormClass.getDataTable() '  Refresh DataTable 
        mdlV_CurrentForm_TotalRecords = FormDataTable.Rows.Count()
        If lblPrimaryKey.Text = "" Then l_CurrentRowID = FormDataTable.Rows.Count() - 1 ' If new record go to last record
        Call UndoAction()
    End Sub
    '
    ' DELETE RECORD FROM TABLE; Set IsDelete Flag True
    '
    Public Sub DeleteRecord()
        If MsgBox("Are you sure you want to delete this record?", MsgBoxStyle.Critical + MsgBoxStyle.YesNo, "WARNING") = MsgBoxResult.Yes Then
            If (objFormClass.deleteRecord(Convert.ToDecimal(lblPrimaryKey.Text))) Then

                '
                ' Set Other/Next Record after deleted
                '
                FormDataTable.Clear()
                FormDataTable = objFormClass.getDataTable() '  Refresh DataTable 
                With FormDataTable
                    If .Rows.Count <> 0 Then
                        If l_CurrentRowID > .Rows.Count - 1 Then l_CurrentRowID -= 1
                        Call GetFormData(FormDataTable, l_CurrentRowID)
                    Else
                        l_CurrentRowID = -1 ' DataTable is Blank
                    End If
                    '
                    'Set value for toolbar action buttons
                    mdlV_CurrentForm_TotalRecords = FormDataTable.Rows.Count
                End With
                MsgBox("Record Delete Successfuly.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
            End If
        End If
    End Sub
#End Region

    Private Sub DGView_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles DGView.CellValueChanged
        Try
            If Me.DGView.Rows.Count > 1 Then

                If e.ColumnIndex > 2 And e.ColumnIndex < 7 Then

                    With Me.DGView.Rows(e.RowIndex)


                        'If .Cells(3).ColumnIndex > 2 And .Selected.Then Then

                        'End If
                        If Not IsDBNull(.Cells(5).Value) Then
                            If .Cells(5).Value = 1 Then
                                .Cells(7).Value = .Cells(3).Value * (.Cells(4).Value - .Cells(6).Value) / 2
                            Else
                                If .Cells(5).Value = Nothing Or .Cells(5).Value = 0 Then
                                    .Cells(6).Value = 0
                                End If
                                .Cells(7).Value = .Cells(3).Value * .Cells(4).Value / 2
                            End If

                        Else
                            .Cells(7).Value = .Cells(3).Value * .Cells(4).Value / 2
                        End If



                    End With

                    Me.txtPuchaseAmount.Text = 0
                    For iLoop As Integer = 0 To Me.DGView.Rows.Count - 2
                        'Me.txtPuchaseAmount.Text = CStr(Val(Me.txtPuchaseAmount.Text) + Val(Me.DGView.Rows(iLoop).Cells(7).Value)).ToString()
                        Me.txtPuchaseAmount.Text = CDbl(Me.txtPuchaseAmount.Text) + CDbl(Me.DGView.Rows(iLoop).Cells(7).Value)
                    Next
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub DGView_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles DGView.CellFormatting
        If e.ColumnIndex = 3 Or e.ColumnIndex = 4 Or e.ColumnIndex >= 6 Then
            If Not IsDBNull(e.Value) Then
                e.Value = Format(CDbl(e.Value), "###,###,##0")
            End If


            'DGView.Columns(e.ColumnIndex).DefaultCellStyle.Format = "#.###"

        End If
    End Sub

    Private Sub DGView_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles DGView.EditingControlShowing


        If Me.DGView.CurrentCell.ColumnIndex = 3 Or Me.DGView.CurrentCell.ColumnIndex = 4 Or Me.DGView.CurrentCell.ColumnIndex = 6 Then
            Dim _txtNum As TextBox = e.Control
            RemoveHandler _txtNum.KeyPress, AddressOf KeyPress_GridViewCell
            AddHandler _txtNum.KeyPress, AddressOf KeyPress_GridViewCell
        End If
    End Sub

    Private Sub KeyPress_GridViewCell(sender As Object, e As KeyPressEventArgs) ' Handles DGView.KeyPress
        Call KeyPressNumeric(e)
    End Sub

    Private Sub txtPuchaseAmount_TextChanged(sender As Object, e As EventArgs) Handles txtPuchaseAmount.TextChanged
        'Dim temp As Double = Me.txtPuchaseAmount.Text
        'Me.txtPuchaseAmount.Text = Format(temp, "###,###,##0")
        If Me.txtPuchaseAmount.Text = 0 Then Exit Sub
        Me.txtPuchaseAmount.Text = Format(CDbl(Me.txtPuchaseAmount.Text), "###,###,##0")
    End Sub

    Private Sub DGView_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles DGView.DataError
        e.Cancel = True
    End Sub

    Private Sub DGView_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles DGView.RowsAdded

        ' MsgBox(sender.Rows(e.RowIndex).Cells(1).Value)

        'DGView.Rows(e.RowIndex).Cells(0)

        CType(sender(0, e.RowIndex), DataGridViewComboBoxCell).Value = CInt(sender.Rows(e.RowIndex).Cells(0).Value)
        CType(sender(1, e.RowIndex), DataGridViewComboBoxCell).Value = sender.Rows(e.RowIndex).Cells(1).Value
        CType(sender(2, e.RowIndex), DataGridViewComboBoxCell).Value = CInt(sender.Rows(e.RowIndex).Cells(2).Value)
        CType(sender(5, e.RowIndex), DataGridViewComboBoxCell).Value = CInt(sender.Rows(e.RowIndex).Cells(5).Value)
    End Sub

    Private Sub grpMain_Enter(sender As Object, e As EventArgs) Handles grpMain.Enter

    End Sub

    Private Sub DGView_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGView.CellContentClick

    End Sub
End Class