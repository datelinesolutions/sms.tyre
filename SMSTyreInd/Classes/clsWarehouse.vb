﻿Public Class clsWarehouse

#Region "Variables and values assign to form class. cp_ :Class Public"
    Dim objDatabase As New clsDatabase
    Dim cp_dataTableQuery As String = "SELECT * FROM vw_WarehouseMaster WHERE isNull(isDeleted, 0) = 0"
    Public cp_tableName As String = "vw_WarehouseMaster"
    Public cp_primaryKey As String = "WarehouseID"
    Public cp_formTitle As String = "Warehouse Master"
    Public cp_searchQuery As String = "SELECT * FROM vw_Find_WarehouseMaster ORDER BY 1"
#End Region

#Region "Public Functions"

    Public Function getDataTable() As DataTable
        getDataTable = objDatabase.getDataTable(cp_dataTableQuery)
    End Function
    Public Function getDataTable(ByVal l_Query As String) As DataTable
        getDataTable = objDatabase.getDataTable(l_Query)
    End Function

    Public Function saveRecorde(ByVal xDataTable As DataTable, ByVal xArrayList As ArrayList) As Boolean
        saveRecorde = objDatabase.saveRecordInTable(cp_tableName, xArrayList, cp_primaryKey)
    End Function
    Public Function editRecord(ByVal _xDataTable As DataTable, ByVal xArrayList As ArrayList, ByVal EditValue As Integer) As Boolean
        editRecord = objDatabase.updateRecordInTable(cp_tableName, xArrayList, cp_primaryKey, EditValue)
    End Function
    Public Function deleteRecord(ByVal xDeleteID As Integer) As Boolean
        Dim l_query As String = ""
        l_query = "UPDATE " & cp_tableName & " SET IsDeleted = 1, ModifiedDate = '" & Now.Date().ToString() & "' WHERE " & cp_primaryKey & "= " & xDeleteID
        deleteRecord = objDatabase.executeDeleteQuery(l_query)
    End Function
#End Region
End Class
