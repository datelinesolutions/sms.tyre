Imports System.Configuration.ConfigurationSettings
Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class clsDatabase
#Region "Public Functions"

    Dim tbl As New DataTable
    '
    '
    '
    Public Function GF_Connection() As SqlConnection
        Dim lConn As New SqlConnection
        'Dim oConfig As Configuration.ConfigurationSettings
        Try
            'Compile for deployment 
            lConn.ConnectionString = My.Settings.ConnectionString

            'Connection String For Deployment
            'lConn.ConnectionString = "Data Source=SERVER-DB2;Database=FKI;UID=sa;pwd=Citrix786"

            ' Connection String For Development (RH)
            'lConn.ConnectionString = "Data Source=ABC;Database=FKI;UID=sa;pwd=pso"

            'lConn.ConnectionString = oConfig.AppSettings("ConnectionString")
            lConn.Open()
            GF_Connection = lConn
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    '
    '
    '
    Public Function GF_ExecuteQry(ByVal sql As String) As Int16
        Dim cmd As New SqlCommand
        Try
            cmd.Connection = GF_Connection()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sql
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
            Throw ex
        Finally
            cmd.Connection.Close()
        End Try
    End Function
    '
    '
    '
    Public Function GF_DataTable(ByVal sql As String) As DataTable
        Dim cmd As New SqlCommand
        Dim lDT As New DataTable
        Try
            cmd.Connection = GF_Connection()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sql
            Dim lDA As New SqlDataAdapter(cmd)
            lDA.Fill(lDT)
            GF_DataTable = lDT
        Catch ex As Exception
            Throw ex
        Finally
            cmd.Connection.Close()
        End Try
    End Function
    '
    ' GET Table data in ADO.Net DataTale Object
    '
    Public Function getDataTable(ByVal sql As String) As DataTable
        Dim cmd As New SqlCommand
        Dim lDT As New DataTable
        Try
            cmd.Connection = GF_Connection()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sql
            Dim lDA As New SqlDataAdapter(cmd)
            lDA.Fill(lDT)
            getDataTable = lDT
        Catch ex As Exception
            Throw ex
        Finally
            cmd.Connection.Close()
        End Try
    End Function
    '
    ' This functioin update record in provide table from FORM
    '
    Public Function updateRecordInTable(ByVal xTableName As String, _
                                        ByVal xFieldList As ArrayList, _
                                        ByVal PrimaryKey As String, _
                                        ByVal EditValue As Integer, _
                                        Optional ByVal xArrayListDetail As ArrayList = Nothing, _
                                        Optional ByVal xQuery As String = Nothing _
    ) As Boolean
        Try

            Dim sConn As String = My.Settings.ConnectionString
            Dim objConn As New SqlConnection(sConn)
            objConn.Open()

            ' Create an instance of a DataAdapter.
            Dim xDataAdapter As _
                New SqlDataAdapter("SELECT * FROM " & xTableName & " WHERE " & PrimaryKey & " = " & CStr(EditValue), objConn)

            ' Create an instance of a DataSet, and retrieve data from the Authors table.
            Dim dsPubs As New DataSet("Pubs")
            xDataAdapter.FillSchema(dsPubs, SchemaType.Source, xTableName)
            xDataAdapter.Fill(dsPubs, xTableName)

            '*****************
            'BEGIN ADD CODE 
            ' Create a new instance of a DataTable.
            Dim xDataTable As DataTable
            xDataTable = dsPubs.Tables(xTableName)

            Dim xNewRow As DataRow
            ' Obtain a new DataRow object from the DataTable.
            '
            'BEGIN EDIT CODE 
            '
            xNewRow = xDataTable.Rows.Find(EditValue)
            xNewRow.BeginEdit()
            For Each xIndex In xFieldList
                xIndex = CType(xIndex, Field)
                If PrimaryKey <> xIndex.fieldName Then
                    xNewRow(xIndex.fieldName) = xIndex.fieldValue
                End If
            Next
            xNewRow.EndEdit()
            '
            'END EDIT CODE  
            '
            '
            'BEGIN SEND CHANGES TO SQL SERVER 
            '
            Dim objCommandBuilder As New SqlCommandBuilder(xDataAdapter)
            xDataAdapter.Update(dsPubs, xTableName)
            '
            'Detail record save/update in table
            '
            If Not xArrayListDetail Is Nothing Then
                'executeQuery("UPDATE tbl_WEA_YarnRequisitionDetail SET IsDeleted =1 WHERE sysYarnRequisitionMasterNo = " + xArrayListDetail(0)(xArrayListDetail(0).Length - 1).ToString)
                updateRecordInTable = saveRecordInDetailTable(xArrayListDetail, xQuery)

            End If


            ' END SEND CHANGES TO SQL SERVER
            updateRecordInTable = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
            updateRecordInTable = False
        End Try

    End Function
    '
    ' This funcation is adding record in provided table from FORM
    '
    Public Function saveRecordInTable(ByVal xTableName As String, _
                                      ByVal xFieldList As ArrayList, _
                                      ByVal PrimaryKey As String, _
                                      Optional ByVal xArrayListDetail As ArrayList = Nothing, _
                                      Optional ByVal xQuery As String = Nothing _
    ) As Boolean
        Try

            Dim sConn As String = My.Settings.ConnectionString
            Dim objConn As New SqlConnection(sConn)
            objConn.Open()

            ' Create an instance of a DataAdapter.
            Dim xDataAdapter As _
                New SqlDataAdapter("SELECT * FROM " & xTableName & " WHERE " & PrimaryKey & " = 0", objConn)

            ' Create an instance of a DataSet, and retrieve data from the Authors table.
            Dim dsPubs As New DataSet("Pubs")
            xDataAdapter.FillSchema(dsPubs, SchemaType.Source, xTableName)
            xDataAdapter.Fill(dsPubs, xTableName)


            '*****************
            'BEGIN ADD CODE 
            ' Create a new instance of a DataTable.
            Dim xDataTable As DataTable
            xDataTable = dsPubs.Tables(xTableName)

            Dim xNewRow As DataRow
            ' Obtain a new DataRow object from the DataTable.
            xNewRow = xDataTable.NewRow()

            '' Set the DataRow field values as necessary.
            For Each xIndex In xFieldList
                xIndex = CType(xIndex, Field)
                If PrimaryKey <> xIndex.fieldName Then
                    xNewRow(xIndex.fieldName) = xIndex.fieldValue
                End If
            Next

            'Pass that new object into the Add method of the DataTable.Rows collection.
            xDataTable.Rows.Add(xNewRow)
            'MsgBox("Add was successful.")

            'END ADD CODE 

            '*****************
            'BEGIN SEND CHANGES TO SQL SERVER 
            Dim objCommandBuilder As New SqlCommandBuilder(xDataAdapter)
            xDataAdapter.Update(dsPubs, xTableName)
            'MsgBox("SQL Server updated successfully" & Chr(13) & "Check Server explorer to see changes")

            'xDataAdapter.SelectCommand("SELECT SCOPE_IDENTITY()")
            '
            'Detail record save/update in table
            '

            If Not xArrayListDetail Is Nothing Then
                Dim getPrimaryKey As String
                getPrimaryKey = getDataTable("SELECT max(" & PrimaryKey & ") FROM " & xTableName).Rows(0).Item(0).ToString()
                saveRecordInTable = saveRecordInDetailTable(xArrayListDetail, xQuery, getPrimaryKey)
            End If



            ' END SEND CHANGES TO SQL SERVER
            saveRecordInTable = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
            saveRecordInTable = False
        End Try

    End Function
    '
    ' This function is adding records in provided detail table from FORM/Grid
    '
    Public Function saveRecordInDetailTable(ByVal xArrayList As ArrayList, _
                                            ByVal xQuery As String, _
                                            Optional ByVal xPrimaryKey As Integer = 0 _
    ) As Boolean
        Try
            Dim _commandBuilder As SqlCommandBuilder
            Dim _dataAdapter As SqlDataAdapter
            Dim _dataTable As DataTable

            Dim iLoop As Integer
            Dim oRow As New Object

            _dataAdapter = New SqlDataAdapter(xQuery.ToString, GF_Connection)
            _commandBuilder = New SqlCommandBuilder(_dataAdapter)
            _dataTable = New DataTable
            _dataAdapter.FillSchema(_dataTable, SchemaType.Source)
            _dataAdapter.Fill(_dataTable)


            If xPrimaryKey <> 0 Then
                'set index for masterNO 
                For iLoop = 0 To xArrayList.Count - 1
                    xArrayList(iLoop)(xArrayList(iLoop).Length - 1) = xPrimaryKey
                Next

            End If

            For iLoop = 0 To xArrayList.Count - 1

                oRow = CType(xArrayList(iLoop), Object)
                ' _dataTable.LoadDataRow(oRow, LoadOption.OverwriteChanges)

                '_dataTable.BeginLoadData()

                _dataTable.LoadDataRow(oRow, LoadOption.Upsert)
               ' _dataTable.EndLoadData()
                ' '_dataTable.AcceptChanges()
            Next


            If GF_Connection.State.Equals(ConnectionState.Closed) Then GF_Connection()
            _dataAdapter.Update(_dataTable)
            GF_Connection.Close()
            _dataTable.AcceptChanges()
            '
            '
            '
            saveRecordInDetailTable = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
            saveRecordInDetailTable = False
        End Try
    End Function
    '
    ' This function use for update record, Like update/delete record in SQL query
    '
    Public Function executeQuery(ByVal l_query As String) As Boolean
        Dim cmd As New SqlCommand
        Try
            cmd.Connection = GF_Connection()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = l_query
            'cmd.ExecuteScalar()
            executeQuery = cmd.ExecuteNonQuery()

        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
            executeQuery = False
            Throw ex
        Finally
            cmd.Connection.Close()
        End Try
    End Function
    '
    ' Getting Primary Key/Identity value from recently added record in table
    '
    Public Function executeQueryGetIdentityValue(ByVal _query As String) As Int64
        Dim cmd As New SqlCommand
        Try
            _query += "; SELECT SCOPE_IDENTITY()"

            cmd.Connection = GF_Connection()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = _query
            'executeQueryGetIdentityValue = cmd.ExecuteNonQuery()
            executeQueryGetIdentityValue = cmd.ExecuteScalar()
        Catch ex As Exception
            MsgBox(ex.Message)
            executeQueryGetIdentityValue = 0
            Throw ex
        Finally
            cmd.Connection.Close()
        End Try
    End Function
    '
    ' This function for update isDelete column in table, for deleting flax
    '
    Public Function executeDeleteQuery(ByVal l_query As String) As Boolean
        Dim cmd As New SqlCommand
        Try
            cmd.Connection = GF_Connection()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = l_query
            'cmd.ExecuteScalar()
            executeDeleteQuery = cmd.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
            executeDeleteQuery = False
        Finally
            cmd.Connection.Close()
        End Try
    End Function
    '
    'Return dataset for find/search form
    '
    Public Function getFindFormDataset(ByVal sql As String) As DataSet
        Dim cmd As New SqlCommand
        Dim lDS As New DataSet
        Try
            cmd.Connection = GF_Connection()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sql
            Dim lDA As New SqlDataAdapter(cmd)
            lDA.Fill(lDS)
            getFindFormDataset = lDS
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
        Finally
            cmd.Connection.Close()
        End Try
    End Function
    '
    '
    '
    Public Function GF_DataTable(ByVal cmd As SqlCommand) As DataTable
        Dim lDT As New DataTable
        Try
            cmd.Connection = GF_Connection()
            Dim lDA As New SqlDataAdapter(cmd)
            lDA.Fill(lDT)
            GF_DataTable = lDT
        Catch ex As Exception
            Throw ex
        Finally
            cmd.Connection.Close()
        End Try
    End Function
    '
    '
    '
    Public Function GF_Parameter(ByVal sParameterName As String, _
                                 ByVal oParameterValue As Object, _
                                 ByVal pParemterDierection As ParameterDirection) As SqlParameter

        Try
            Dim lSqlParm As New SqlParameter
            lSqlParm.ParameterName = sParameterName
            lSqlParm.Value = oParameterValue
            lSqlParm.Direction = pParemterDierection
            GF_Parameter = lSqlParm
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    '
    '
    '
    Public Function GF_GetMaximumNumber(ByVal Str As String, ByVal TableName As String, ByVal ColName As String, Optional ByVal Condition As String = "", Optional ByVal DocumentValue As String = "") As String
        Try
            Dim xValue, GenStr As String
            Dim xPos, xPos2, xNo As Integer

            Dim DT As New DataTable

            DT = GF_DataTable("select * from GEN_GeneralSetup where TableName = '" & TableName & "'" & Condition)

            If Str.IndexOf("where") = -1 Then
                Str = Str & " where len(" & DT.Rows(0).Item("ColumnName") & ") = " & DT.Rows(0).Item("ColumnSize") & Condition
            End If

            tbl = GF_DataTable(Str)

            If IsDBNull(tbl.Rows(0).Item(ColName)) Then
                xValue = "0"
            Else
                xValue = tbl.Rows(0).Item(ColName)
            End If

            If DocumentValue <> "" Then
                xValue = DocumentValue
            End If
            If xValue = "0" Then      'Bigining of File
                GF_GetMaximumNumber = DT.Rows(0).Item("InitialValue")
            Else
                If xValue.Length = DT.Rows(0).Item("ColumnSize") Then
                    xPos2 = xValue.Length
                    For xPos = 1 To xValue.Length
                        If Not IsNumeric(Right(xValue, xPos)) Or Left(Right(xValue, xPos), 1) = "-" Then
                            xPos2 = xPos
                            Exit For
                        End If
                    Next
                    If xPos2 = 1 Then
                        GF_GetMaximumNumber = ""
                    Else
                        xNo = Right(xValue, (xPos2 - 1)) + DT.Rows(0).Item("Increment")
                        GF_GetMaximumNumber = Left(xValue, xValue.Length - (xPos2 - 1)) & GF_Lpad(xNo, xPos2 - 1, "0")
                    End If
                Else
                    GF_GetMaximumNumber = DT.Rows(0).Item("InitialValue")
                End If
            End If
            GF_GetMaximumNumber = ChkMaxNumber(Str, TableName, ColName, Condition, GF_GetMaximumNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    '
    '
    '
    Function ChkMaxNumber(ByVal Str As String, ByVal TableName As String, ByVal ColName As String, Optional ByVal Condition As String = "", Optional ByVal DocumentValue As String = "") As String
        Try
            tbl = GF_DataTable(" SELECT OBJECT_ID('" & TableName & "_LOG','U')")
            If Not IsDBNull(tbl.Rows(0).Item(0)) Then
                tbl = GF_DataTable(" Select * from  " & TableName & "_LOG Where " & ColName & "='" & DocumentValue & "'")
                If tbl.Rows.Count = 0 Then
                    ChkMaxNumber = DocumentValue
                Else
                    ChkMaxNumber = GF_GetMaximumNumber(Str, TableName, ColName, Condition, DocumentValue)
                    'txtDocNo_MDY.Text = sval
                End If
            Else
                ChkMaxNumber = DocumentValue
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    '
    '
    '
    Function GF_Rpad(ByVal xStr As Object, ByVal xNo As Integer, ByVal Pad As String) As String
        Dim xxStr, x As Integer
        Dim xChr As String

        xxStr = xNo - Len(Trim(xStr))
        For x = 1 To xxStr
            xChr = xChr & Pad
        Next
        GF_Rpad = xStr & xChr
    End Function
    '
    '
    '
    Function GF_Lpad(ByVal xStr As Object, ByVal xNo As Integer, ByVal Pad As String) As String
        Dim xxStr, x As Integer
        Dim xChr As String

        xxStr = xNo - Len(Trim(xStr))
        For x = 1 To xxStr
            xChr = xChr & Pad
        Next
        GF_Lpad = xChr & xStr
    End Function
    '
    '
    '
    Public Function CrystalDate(ByVal dt As Date) As String
        CrystalDate = Format(dt, "yyyy,MM,dd")
    End Function
    '
    '
    '
    Public Function SQLDate(ByVal dt As Date) As String
        SQLDate = Format(dt, "MM/dd/yyyy")
    End Function

    ''
    ''SaveMe("GEN_LISTOFVALUES", "sysCode", oclsDB.GF_Connection, grd)
    ''
    'Private Sub SaveMe(ByVal tblName As String, ByVal pkName As String, ByVal Connection As SqlConnection, ByVal grd As C1.Win.C1FlexGrid.C1FlexGrid)

    '    Dim FieldName As String = Nothing
    '    Dim Sql As String
    '    Dim c_Builder As SqlCommandBuilder
    '    Dim d_Adapter As SqlDataAdapter
    '    Dim DT_Detail As New DataTable

    '    Try

    '        For Each grdcol As C1.Win.C1FlexGrid.Column In grd.Cols
    '            If Not (grdcol.Name.ToString.Length.Equals(0)) Then
    '                FieldName += grdcol.Name + ","
    '            End If
    '        Next
    '        FieldName = FieldName.ToString.Substring(0, FieldName.ToString.Length - 1)

    '        ' retriving table structure 
    '        ' and SETTING database to Insertion
    '        'Sql = "SELECT " + FieldName.ToString + " ,isDeleted,sysUserCode,LastModifiedDate FROM " + tblName.ToString + " WHERE " + pkName.ToString + " IS NULL "
    '        Sql = "SELECT " + FieldName.ToString + "  FROM " + tblName.ToString + " WHERE " + pkName.ToString + " IS NULL "

    '        d_Adapter = New SqlDataAdapter(Sql.ToString, Connection)
    '        c_Builder = New SqlCommandBuilder(d_Adapter)
    '        DT_Detail = New DataTable
    '        d_Adapter.FillSchema(DT_Detail, SchemaType.Source)
    '        d_Adapter.Fill(DT_Detail)


    '        Dim FieldValues(FieldName.Split(",").Length - 1) As Object

    '        Dim Field As Object
    '        Dim iCol As Integer
    '        Dim iRow As Integer = 0

    '        With grd
    '            For iRow = 1 To .Rows.Count - 1
    '                iCol = 0
    '                For Each grdcol As C1.Win.C1FlexGrid.Column In grd.Cols
    '                    If (.GetData(iRow, grdcol.Index) = (Nothing)) Then
    '                        Exit For
    '                    Else
    '                        If Not (grdcol.Name.ToString.Length.Equals(0)) Then
    '                            FieldValues(iCol) = grd.GetData(iRow, grdcol.Index)
    '                            iCol += 1
    '                        End If
    '                    End If
    '                Next
    '                DT_Detail.LoadDataRow(FieldValues, LoadOption.Upsert)
    '            Next
    '        End With

    '        If Connection.State.Equals(ConnectionState.Closed) Then Connection.Open()
    '        d_Adapter.Update(DT_Detail)
    '        Connection.Close()
    '        DT_Detail.AcceptChanges()

    '    Catch ex As Exception

    '    End Try
    'End Sub


#End Region
End Class
Public Class clsFoxProDB
    Dim con As OleDbConnection
    Public dr As OleDbDataReader
    Public DSname As String
    Public Function GF_Connection() As OleDbConnection
        Dim ConnString As String = "Provider=VFPOLEDB.1;DSN=" & DSname.Trim & ""
        con = New OleDbConnection(ConnString)
        Try
            con.Open()
            GF_Connection = con
        Catch e As Exception
            MsgBox(e.ToString)
        End Try
    End Function
    Public Function GF_DataReader(ByVal sql As String) As OleDbDataReader
        Dim cmd As New OleDbCommand
        Dim lDT As New DataTable
        Try
            cmd.Connection = GF_Connection()

            cmd.CommandType = CommandType.Text
            cmd.CommandText = sql
            dr = cmd.ExecuteReader()

            GF_DataReader = dr
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
'
'Below are Genral Class
'
Public Class Field
    Public fieldName As String
    Public fieldValue As String
End Class

