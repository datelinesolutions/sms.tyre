﻿Public Class clsPurchaseOrder

#Region "Variables and values assign to form class. cp_:Class Public"
    Dim objDatabase As New clsDatabase
    Dim cp_dataTableQuery As String = "SELECT * FROM vw_PurchaseMaster WHERE isNull(isDeleted, 0) = 0"
    '
    'Detail Table Information
    Dim cp_dataTableQuery_Detail As String = "SELECT * FROM vw_GetPurchaseDetail WHERE PurchaseID ="
    Public cp_detail_Table As String = "vw_PurchaseDetails"
    Public cp_detail_Fields As String = "BrandID,SizeID,PatternID,Quantity,Rate,GiveUpID,GiveupRate,PurchaseDetailID,PurchaseID"

    Public cp_tableName As String = "vw_PurchaseMaster"
    Public cp_primaryKey As String = "PurchaseID"
    Public cp_formTitle As String = "Purchase Order"
    Public cp_searchQuery As String = "SELECT * FROM vw_Find_Purchase ORDER BY 1 DESC"
    Public cp_isMaster As Boolean = False
#End Region

#Region "Public Functions"

    Public Function getDataTable() As DataTable
        getDataTable = objDatabase.getDataTable(cp_dataTableQuery)
    End Function
    Public Function getDataTable(ByVal l_Query As String) As DataTable
        getDataTable = objDatabase.getDataTable(l_Query)
    End Function
    Public Function getDataTableDetail(ByVal _pkID As Int32) As DataTable
        getDataTableDetail = objDatabase.getDataTable(cp_dataTableQuery_Detail + _pkID.ToString)
    End Function
    Public Function saveRecorde(ByVal xDataTable As DataTable, ByVal xArrayList As ArrayList, Optional ByVal xArrayList_Detail As ArrayList = Nothing) As Boolean
        Dim xQuery As String = "SELECT " + cp_detail_Fields + " FROM " + cp_detail_Table + " WHERE isNull(isDeleted, 0) = 0 AND " + cp_primaryKey + " = 0"
        saveRecorde = objDatabase.saveRecordInTable(cp_tableName, xArrayList, cp_primaryKey, xArrayList_Detail, xQuery)
    End Function
    Public Function editRecord(ByVal _xDataTable As DataTable, ByVal xArrayList As ArrayList, ByVal EditValue As Integer, Optional ByVal xArrayList_Detail As ArrayList = Nothing) As Boolean
        Dim xQuery As String = "SELECT " + cp_detail_Fields + " FROM " + cp_detail_Table + " WHERE isNull(isDeleted, 0) = 0 AND " + cp_primaryKey + " = " + CType(EditValue, String)
        editRecord = objDatabase.updateRecordInTable(cp_tableName, xArrayList, cp_primaryKey, EditValue, xArrayList_Detail, xQuery)
    End Function
    Public Function deleteRecord(ByVal xDeleteID As Integer) As Boolean
        Dim l_query As String = ""
        l_query = "UPDATE " & cp_tableName & " SET IsDeleted = 1, ModifiedDate = '" & Now.Date().ToString() & "' WHERE " & cp_primaryKey & "= " & xDeleteID
        deleteRecord = objDatabase.executeDeleteQuery(l_query)
    End Function
#End Region
End Class
