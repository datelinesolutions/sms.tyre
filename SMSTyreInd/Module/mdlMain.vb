﻿Module mdlMain

    ' for this event

#Region "Public Variables mdl:Module; V:Varibale; C:Constant"

    Public objMainForm As New frmMain       ' main form object
    Public mdlV_searchResult As Integer = 0 ' Global variable for Find Result from lookup form
    Public mdlV_isFormValidate As Boolean = True  ' Global variable for record save status
    Public mdlV_CurrentForm_TotalRecords As Integer = 0 ' Global variable for total records count in current Form


    Public Const mdlV_securityCode As String = "REHAN_HUSSAIN"


    Public mdlV_UserCode As Int64
    Public mdlV_UserName As String
    Public mdlV_DisplayName As String
    Public mdlV_Password As String
    Public mdlV_SessionID As Integer

    Public mdlV_ComputerName As String = System.Net.Dns.GetHostName.ToString()
    Public mdlV_ComputerIP As String = System.Net.Dns.GetHostByName(mdlV_ComputerName).AddressList(0).ToString()
    Public mdlV_NetworkUser As String = Environment.UserDomainName & "\" & Environment.UserName


    Public isLoginSuccessed As Boolean




    Public GV_DelErrNo As Integer = 547




    Public Const ERR_MSG As String = "Some Errors Occured. Please Try Again"
    Public Const DEL_ERR_MSG As String = "Record Can't be Deleted Becuase it has Related Records"
    Public Const VALID_MSG As String = "Following Fields are Missing"
    Public Const NUMERIC_MSG As String = "Please enter numeric data in"
    Public Const ADD_MSG As String = "Do you want add this."
    Public Const DATE_MSG As String = "Ending Date is Less than or equal to Starting Date."

    Public GV_UserName As String
    Public GV_UserID As Integer
    Public GV_DeptID As Integer
    Public GV_Deptartment As String
    Public GV_UserType As String
    Public GV_UserPwd As String
    Public GV_ChkRemember As String
    Public GV_Dept As String = "IT"
    Public GV_CompanyName As String = "RDM"

    ' Public oclsUtilities As New clsUtilities

    Public GV_ServerName As String = "FKI"
    Public GV_DBName As String = "FKI"
    Public GV_LookUpBtn As String = "cmdLookUp"
    Public GV_ComputerName As String = My.Computer.Name
    Public GV_ConnectionString As String = My.Settings.ConnectionString



#End Region

    Public Sub main()
        'mdlV_ComputerIP = System.Net.Dns.GetHostEntry(mdlV_ComputerName).AddressList(0).ToString()
        'mdlV_ComputerIP = System.Net.Dns.GetHostByName(mdlV_ComputerName).AddressList(0).ToString()

        ' Call mdl_SplashForm() ' Show Splash Form

        'System.Windows.Forms.Application.Run(New frmLogin)

        Try
            'Dim ofrmLogin As New frmLogin
            'Dim ofrmLogin As New frmMain

            'ofrmLogin.Icon = objMainForm.Icon
            'ofrmLogin.AcceptButton = ofrmLogin.cmdOk
            'ofrmLogin.CancelButton = ofrmLogin.cmdCancel
            'ofrmLogin.Show()
            'If isLoginSuccessed Then
            'OfrmMain.Show()
            objMainForm.Text = "SMS for Tyre Shop"
            Application.Run(objMainForm)
            'End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

#Region "Form's Controls Funcations/Proedures "
    '
    '
    '
    Public Sub mdl_showMDIChild(ByVal frmChild As Form) 'As Integer
        Try
            Dim frm As Form
            For Each frm In objMainForm.MdiChildren
                If frm.Name = frmChild.Name And frm.Tag = frmChild.Tag Then
                    frm.Activate()
                    Exit Sub
                End If
            Next
            'frmChild.Text = "RDM"
            'frmChild.Icon = objMainForm.Icon
            frmChild.MdiParent = objMainForm
            'frmChild.StartPosition = FormStartPosition.CenterScreen
            frmChild.StartPosition = FormStartPosition.CenterParent

            frmChild.Show()
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Information)
        End Try
    End Sub
    '
    ' This Procedure Fills Form Contral Data like TextBox and etc. from DataTable
    '
    Public Sub mdl_PopulateFormControls(ByVal l_DataTable As DataTable, _
                                        ByVal l_RecordIndex As Integer, _
                                        ByVal GroupBox As Windows.Forms.GroupBox, _
                                        Optional ByVal l_DataTable_Detail As DataTable = Nothing, _
                                        Optional ByVal l_GridColumnCounts As Integer = Nothing _
    )
        Dim cControl As Control
        Try
            For Each cControl In GroupBox.Controls
                If (TypeOf cControl Is TextBox) Or (TypeOf cControl Is Label) Then
                    If cControl.Tag <> "" Then
                        cControl.Text = IIf(IsDBNull(l_DataTable.Rows(l_RecordIndex)(cControl.Tag)), "", l_DataTable.Rows(l_RecordIndex)(cControl.Tag))
                    End If
                ElseIf (TypeOf cControl Is CheckBox) Then
                    CType(cControl, CheckBox).Checked = IIf(IsDBNull(l_DataTable.Rows(l_RecordIndex)(cControl.Tag)), False, l_DataTable.Rows(l_RecordIndex)(cControl.Tag))
                ElseIf (TypeOf cControl Is ComboBox) Then
                    CType(cControl, ComboBox).SelectedValue = l_DataTable.Rows(l_RecordIndex)(cControl.Tag)
                ElseIf (TypeOf cControl Is DateTimePicker) Then
                    CType(cControl, DateTimePicker).Value = l_DataTable.Rows(l_RecordIndex)(cControl.Tag)
                ElseIf (TypeOf cControl Is DataGridView) And cControl.Name = "DGView" Then
                    '
                    ' For Master details Form
                    '
                    If Not l_DataTable_Detail Is Nothing Then

                        Dim i, iCol, iRow, rowIndex As Int16
                        Dim dgRow As DataGridViewRow

                        Dim CBCell As DataGridViewComboBoxCell
                        Dim CBCell_ As DataGridViewComboBoxColumn


                        iRow = 1

                        With CType(cControl, DataGridView)

                            .Rows.Clear()
                            '.Rows.Clear()
                            'If Not .Rows.Count.Equals(2) Then
                            '    .Rows.RemoveRange(1, .Rows.Count - 2)
                            '    '.Rows.RemoveRange(1, 1)
                            'End If
                            'For Each dtRow As DataRow In l_DataTable_Detail.Rows

                            For Each dtRow As DataRow In l_DataTable_Detail.Rows

                                'Dim objectName As Object() = New Object(1) {}


                                ' .Rows.Add(CType(dtRow.ItemArray, Object()))
                                .Rows.Add(dtRow.ItemArray)

                                '.Rows.Add()
                                'CBCell = .Rows(rowIndex).Cells(0)
                                'CBCell.Value = dtRow.Item(0)
                                'CBCell.Items.Add(l_DataTable_Detail.Rows(rowIndex)(0).ToString)


                                '.Rows(rowIndex).Cells(0).Value = 2

                                '.Rows(rowIndex).Cells(1).Value = 1
                                '.Rows.Add(

                                'rowIndex = .Rows.Count - 1
                                'CType(.Rows(rowIndex).Cells(0), DataGridViewComboBoxCell).Value = l_DataTable_Detail.Rows(rowIndex)(0).ToString
                                ' CType(sender(0, e.RowIndex), DataGridViewComboBoxCell).Value


                                '.Rows.Add()
                                'rowIndex = .Rows.Count - 1
                                'CType(.Rows(rowIndex).Cells(0), DataGridViewComboBoxCell).Value = CInt(dtRow.Item(0).ToString)





                            Next


                            'For i = 0 To l_DataTable_Detail.Rows.Count - 1
                            '    '.Rows.Add()
                            '    'For iCol = 0 To .Columns.Count - 1
                            '    '    If .cells(iCol).GetType Then

                            '    '    End If


                            '    .Rows.Add(l_DataTable_Detail.Rows(i))

                            '    '    '.SetData(i + 1, iCol, l_DataTable_Detail.Rows(i)(iCol).ToString)
                            '    'Next
                            '    '.Rows.Add()
                            'Next

                            '.DataSource = l_DataTable_Detail
                        End With
                    End If
                End If
            Next cControl
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    '
    ' This procedure pick-up values from FORM contrals into a array for update/save record in database
    '
    Public Function mdl_PopulateDataColumns(ByVal GroupBox As Windows.Forms.GroupBox) As ArrayList
        Dim objField As Field
        Dim xArray As New ArrayList

        Dim cControl As Control
        For Each cControl In GroupBox.Controls
            'If (TypeOf cControl Is TextBox) Or (TypeOf cControl Is Label) Or (TypeOf cControl Is CheckBox) Then
            If (TypeOf cControl Is TextBox) Or (TypeOf cControl Is Label) Then
                If cControl.Tag <> "" And Not (cControl.Name.Contains("_dsp")) Then
                    objField = New Field()
                    objField.fieldName = cControl.Tag.Trim
                    objField.fieldValue = cControl.Text.Trim
                    'objField.fieldValue = IIf((TypeOf cControl Is CheckBox), CType(cControl, CheckBox).Checked, cControl.Text.Trim)
                    xArray.Add(objField)
                End If
            ElseIf (TypeOf cControl Is DateTimePicker) Then
                objField = New Field()
                objField.fieldName = cControl.Tag.Trim
                objField.fieldValue = CType(cControl, DateTimePicker).Value
                xArray.Add(objField)
            ElseIf (TypeOf cControl Is ComboBox) Then
                objField = New Field()
                objField.fieldName = cControl.Tag.Trim
                objField.fieldValue = CType(cControl, ComboBox).SelectedValue
                xArray.Add(objField)
            End If
        Next cControl
        '
        ' SET Defult Value like User ID and Last Modified Date
        '
        objField = New Field()
        objField.fieldName = "UserID"
        objField.fieldValue = mdlV_UserCode  ' Need Globle Variable for USER ID 
        xArray.Add(objField)
        '
        objField = New Field()
        objField.fieldName = "ModifiedDate"
        objField.fieldValue = Now.Date  ' Last Modify/Add Date
        xArray.Add(objField)
        '
        '
        mdl_PopulateDataColumns = xArray
    End Function
    '
    ' This procedure pick-up values from FORM-Grid into a array for update/save record in detail table
    '
    Public Function mdl_PopulateDetailsTable(ByVal xGridControl As DataGridView, _
                                             ByVal xColumnList As String, _
                                             ByVal xPrimaryKeyName As String, _
                                             ByVal xPrimaryKey As Int64 _
    ) As ArrayList
        Try

            Dim xArray As New ArrayList
            Dim iCol, iColF, iRow As Integer
            'Dim FieldValues(xColumnList.Split(",").Length - 1) As Object
            Dim TestArray() As String = Split(xColumnList.ToString, ",", , CompareMethod.Text)

            With xGridControl
                For iRow = 0 To .Rows.Count - 2

                    Dim FieldValues(xColumnList.Split(",").Length - 1) As Object
                    Dim DGViewRow As DataGridViewRow = .Rows(iRow)

                    'If (.GetData(iRow, .Cols(0).Index) = (Nothing)) Then
                    '    Exit For
                    'End If

                    iCol = 0
                    For iColF = 0 To DGViewRow.Cells.Count - 1
                        If Not .Columns(iColF).Name.ToString.Contains("_dsp") Then

                            If .Columns(iColF).Name.ToString = TestArray(iCol).ToString Then

                                'If DGViewRow.Cells(iColF).Value = Nothing Or DGViewRow.Cells(iColF).Value = "None" Then
                                '    FieldValues(iCol) = 0
                                'Else
                                'FieldValues(iCol) = DGViewRow.Cells(iColF).Value
                                'End If
                                If CStr(DGViewRow.Cells(iColF).Value) = "None" Then
                                    FieldValues(iCol) = "0"
                                Else
                                    'FieldValues(iCol) = IIf(DGViewRow.Cells(iColF).Value = Nothing, "0", DGViewRow.Cells(iColF).Value)
                                    FieldValues(iCol) = DGViewRow.Cells(iColF).Value
                                End If

                                ' FieldValues(iCol) = IIf(DGViewRow.Cells(iColF).Value = Nothing, 0, DGViewRow.Cells(iColF).Value)

                                'If CType(FieldValues(iCol), String) = "True" Or CType(FieldValues(iCol), String) = "False" Then
                                '    If CType(FieldValues(iCol), String) = "True" Then
                                '        FieldValues(iCol) = "1"
                                '    Else
                                '        FieldValues(iCol) = "0"
                                '    End If
                                'End If
                            End If
                            iCol += 1
                        End If
                    Next
                    'If TestArray(iCol) = "IsDeleted" Then
                    '    FieldValues(iCol) = 0
                    'End If
                    If TestArray(iCol) = xPrimaryKeyName Then 'Primary Key
                        FieldValues(iCol) = xPrimaryKey
                    End If
                    '
                    '
                    xArray.Add(FieldValues)
                Next
            End With
            '
            '
            mdl_PopulateDetailsTable = xArray
        Catch ex As Exception
            Throw (ex)
        End Try
    End Function
    '
    '
    '
    Public Function mdl_FindRecordByLookup_class(ByVal _frmSearch As frmFind, ByVal sSearch As String, ByVal sSql As String) As Boolean
        'Dim _Thread As New Threading.Thread(AddressOf _frmSearch.FillGrid)
        '_Thread.Start()
        With _frmSearch
            .KeyPreview = True
            .Icon = objMainForm.Icon
            .Text = sSearch & " Record Search Form"
            .SetFindQuery = sSql
            .StartPosition = FormStartPosition.CenterScreen
            .ShowInTaskbar = False
            .ShowDialog()
        End With

        ''_frmSearch.KeyPreview = True
        ''_frmSearch.Icon = objMainForm.Icon

        ''_frmSearch.lblMain.Text = "Search " & sSearch
        '_frmSearch.SetFindQuery = sSql
        ''_frmSearch.grpMain.Text = _frmSearch.lblMain.Text
        '_frmSearch.StartPosition = FormStartPosition.CenterScreen
        '_frmSearch.ShowInTaskbar = False
        '_frmSearch.ShowDialog()

        mdl_FindRecordByLookup_class = True
    End Function

    Public Sub mdl_EmptyFormControls(ByVal groupBox As Windows.Forms.GroupBox)
        Dim control As New Control

        For Each control In groupBox.Controls
            If (TypeOf control Is TextBox) Or (TypeOf control Is Label) Then
                If control.Tag <> "" Then
                    control.Text = ""
                End If
            ElseIf (TypeOf control Is DateTimePicker) Then
                CType(control, DateTimePicker).Value = Now.ToString()
            ElseIf (TypeOf control Is ComboBox) Then
                CType(control, ComboBox).SelectedValue = -1
            ElseIf (TypeOf control Is DataGridView) Then
                CType(control, DataGridView).Rows.Clear()
            End If
        Next

    End Sub
    '
    'Genric funcion for Form Input Controls validation
    '
    Public Function mdl_FormValidation(ByVal GroupBox As Windows.Forms.GroupBox, Optional ByRef DGView As DataGridView = Nothing) As Boolean
        Dim _result As Boolean = True
        Dim _resultMsg As String = ""
        For Each ctrl As Control In GroupBox.Controls
            If TypeOf ctrl Is TextBox Or TypeOf ctrl Is ListBox Then
                If Right(ctrl.Name, 4) = "_Req" Then
                    If ctrl.Text = "" Then
                        _result = False
                        _resultMsg += IIf(_resultMsg.Length = 0, "", ", ") + ctrl.AccessibleDescription
                    End If
                ElseIf Right(ctrl.Name, 5) = "_ReqN" Then
                    If ctrl.Text = "" Then
                        _result = False
                        _resultMsg += IIf(_resultMsg.Length = 0, "", ", ") + ctrl.AccessibleDescription
                    ElseIf Not IsNumeric(ctrl.Text) Then
                        _result = False
                        _resultMsg += IIf(_resultMsg.Length = 0, "", ", ") + ctrl.AccessibleDescription + "[Nmeric Input]"
                    End If
                End If
                'ElseIf Right(ctrl.Name, 5) = "_ReqN" Then
            End If
        Next
        If Not DGView Is Nothing Then
            If DGView.Rows.Count <= 1 Then
                _result = False
                _resultMsg += "Please enter any one details"
            End If
        End If
        If Not _result Then
            MsgBox(VALID_MSG & vbCrLf & _resultMsg, MsgBoxStyle.Information)
        End If
        Return _result
    End Function
    '
    ' This function is fill form level combobox with provided values/Data Table 
    '
    Public Sub mdl_FillFormCombobox(ByVal l_DataTable As DataTable, ByVal l_Combobox As ComboBox, ByVal l_Value As String, ByVal l_Text As String)
        With l_Combobox
            .DataSource = l_DataTable
            .ValueMember = l_Value
            .DisplayMember = l_Text
        End With
    End Sub
    '
    '
    '
    Public Sub mdl_FillGridCombo(ByRef dgView As DataGridView, _
                                ByVal colIndex As Integer, _
                                ByVal dataTable As DataTable, _
                                ByVal dataPropertyName As String, _
                                ByVal valueMember As String, _
                                ByVal displayMember As String, _
                                ByVal headerText As String, _
                                ByVal width As Integer
    )
        Try
            Dim dgComboBox As DataGridViewComboBoxColumn = New DataGridViewComboBoxColumn()
            With dgComboBox
                .DataPropertyName = dataPropertyName
                .DataSource = dataTable
                .DisplayMember = displayMember
                .ValueMember = valueMember
                .HeaderText = headerText
                .Width = width
                .Name = dataPropertyName

            End With
            With dgView
                .Columns.Remove(dataPropertyName)
                .Columns.Insert(colIndex, dgComboBox)
            End With

            ''Dim hList As New Hashtable
            'Dim hList As New System.Collections.Specialized.ListDictionary
            'Dim iRow As Integer
            'Dim Display As String

            'With lDTSrc
            '    For iRow = 0 To .Rows.Count - 1
            '        If sDisplaySecondField <> "" Then
            '            Display = .Rows(iRow)(sDisplay) & " , " & .Rows(iRow)(sDisplaySecondField) & IIf(IsDBNull(.Rows(iRow)(sDisplayThirdField)), "", " , " & .Rows(iRow)(sDisplayThirdField)) & IIf(IsDBNull(.Rows(iRow)(sDisplayFourthField)), "", " , " & .Rows(iRow)(sDisplayFourthField)) & IIf(IsDBNull(.Rows(iRow)(sDisplayFifthField)), "", " , " & .Rows(iRow)(sDisplayFifthField))
            '            hList.Add(.Rows(iRow)(sID), Display)
            '        Else
            '            hList.Add(.Rows(iRow)(sID), .Rows(iRow)(sDisplay))
            '        End If
            '    Next
            'End With

            'With grd
            '    .Cols(iCol).DataMap = Nothing
            '    .Cols(iCol).DataMap = hList
            'End With

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub
    '
    '
    '
    Public Sub mdl_GridOption(ByRef grd As DataGridView, ByVal xOption As Boolean, ByVal xIsNewRecord As Boolean)
        With grd
            'If xIsNewRecord Then .Rows.RemoveRange(1, .Rows.Count - 2) ' For New Record only

            .AllowUserToAddRows = xOption
            .AllowUserToDeleteRows = xOption
            '.AllowAddNew = xOption
            '.AllowDelete = xOption
        End With
    End Sub
#End Region

#Region "Subroutine and Funcations/Procedures "

    Public Sub KeyPressNumeric(ByVal e As KeyPressEventArgs)
        If Not IsNumeric(e.KeyChar.ToString) And Not e.KeyChar = Chr(Keys.Back) Then
            e.Handled = True
        End If
    End Sub

    Public Sub mdl_ActivedEventToolbar(ByRef mdiForm As Form)
        'Dim mdiForm As Form = frmMain
        Dim m As Type = mdiForm.[GetType]()

        Dim setToolbar As Reflection.MethodInfo = m.GetMethod("setToolbarButtonsState")

        If setToolbar IsNot Nothing Then
            setToolbar.Invoke(mdiForm, Nothing)
        End If
    End Sub

#End Region

End Module
